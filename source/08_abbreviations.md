# Abbreviations {.unnumbered}

\begin{tabbing}
\textbf{AAGAB}~~~~~~~~~~~~ \= \textbf{A}lpha \textbf{A}nd \textbf{G}amma \textbf{A}daptin \textbf{B}inding-Protein \\
\textbf{AI} \> \textbf{A}romatase \textbf{I}nhibitor \\
\textbf{API} \> \textbf{A}pplication \textbf{P}rogramming \textbf{I}nterface \\
\textbf{AURKA} \> \textbf{AUR}ora \textbf{K}inase \textbf{A} \\  
\textbf{BC} \> \textbf{B}reast \textbf{C}ancer \\
\textbf{BRCA} \> \textbf{BR}east \textbf{CA}ncer \\
\textbf{CB} \> \textbf{C}ore-needle \textbf{B}iopsy \\  
\textbf{cfDNA} \> \textbf{c}ell-\textbf{f}ree \textbf{DNA} \\  
\textbf{ctDNA} \> \textbf{c}irculating \textbf{t}umour \textbf{DNA} \\  
\textbf{DGEA} \> \textbf{D}ifferential \textbf{G}ene \textbf{E}xpression \textbf{A}nalysis \\  
\textbf{DNA} \> \textbf{D}eoxyribo \textbf{N}ucleic \textbf{Acid} \\  
\textbf{EB} \> \textbf{E}xcision \textbf{B}iopsy \\  
\textbf{EGFR} \> \textbf{E}pidermal \textbf{G}rowth \textbf{F}actor \textbf{R}eceptor \\  
\textbf{ER} \> \textbf{E}strogen \textbf{R}eceptor \\
\textbf{FF} \> \textbf{F}resh \textbf{F}rozen \\  
\textbf{FFPE} \> \textbf{F}ormalin \textbf{F}ixed \textbf{P}arafin \textbf{E}mbedded \\  
\textbf{GSEA} \> \textbf{G}eneset \textbf{E}nrichment \textbf{A}nalysis \\  
\textbf{HER2} \> \textbf{H}uman \textbf{E}pidermal-Growth-Factor \textbf{R}eceptor \\
\textbf{HR} \> \textbf{H}ormone \textbf{R}eceptor \\  
\textbf{IGF} \> \textbf{I}nsulin like \textbf{G}rowth \textbf{F}actor \\  
\textbf{IHC} \> \textbf{I}mmunohistochemistry \textbf{C}hemical \\  
\textbf{JSON} \> \textbf{J}ava\textbf{S}cript \textbf{O}bject \textbf{N}otation \\
\textbf{LumA} \> \textbf{L}uminal \textbf{A} \\
\textbf{LumB} \> \textbf{L}uminal \textbf{B} \\
\textbf{LFDA} \> \textbf{L}ocal \textbf{F}isher \textbf{D}iscriminant \textbf{A}nalysis \\
\textbf{MAPK} \> \textbf{M}itogen \textbf{A}ctivated \textbf{P}rotein \textbf{K}inase \\  
\textbf{METABRIC} \> \textbf{M}olecular \textbf{T}axonomy \textbf{O}f \textbf{B}reast \textbf{C}ancer \textbf{I}nternational \textbf{C}onsortium \\  
\textbf{MDS} \> \textbf{M}ulti \textbf{D}imensional \textbf{S}caling \\
\textbf{NAC} \> \textbf{N}eo\textbf{a}djuvant \textbf{C}hemotherapy \\
\textbf{NCBIGEO} \> \textbf{N}ational \textbf{C}entre for \textbf{B}iotechnology \textbf{I}nformation \textbf{G}ene \textbf{E}xpression \textbf{O}mnibus \\  
\textbf{NIT} \> \textbf{N}o \textbf{I}ntervening \textbf{T}reatment \\  
\textbf{NKI} \> \textbf{N}ational \textbf{K}aker \textbf{I}nstituut \\  
\textbf{NPI} \> \textbf{N}ottingham \textbf{P}rognostic \textbf{I}ndex \\  
\textbf{OR} \> \textbf{O}dds \textbf{R}atio \\
\textbf{PCA} \> \textbf{P}rincipal \textbf{C}omponent \textbf{A}nalysis \\
\textbf{PCNA} \> \textbf{P}roliferating \textbf{C}hain \textbf{N}uclear \textbf{A}ntigen \\  
\textbf{PCR} \> \textbf{P}athological \textbf{C}omplete \textbf{R}esponse \\  
\textbf{POETIC} \> \textbf{P}eri \textbf{O}perative \textbf{E}ndocrine \textbf{T}herapy for \textbf{I}ndividualising \textbf{C}are \\  
\textbf{PR} \> \textbf{P}rogesterone \textbf{R}eceptor \\
\textbf{RNA} \> \textbf{R}ibonucleic \textbf{A}cid \\  
\textbf{ROR} \> \textbf{R}isk \textbf{O}f \textbf{R}ecurrence \\  
\textbf{SAM} \> \textbf{S}ignificance \textbf{A}nalysis of \textbf{M}icroarrays \\  
\textbf{T2} \> \textbf{T}ime \textbf{2}nd \\
\textbf{TM} \> \textbf{T}ime \textbf{M}id-chemo \\
\textbf{TNBC} \> \textbf{T}riple \textbf{N}egative \textbf{B}reast \textbf{C}ancer \\  
\textbf{TP} \> \textbf{T}ime \textbf{P}re-treatment \\
\textbf{TS} \> \textbf{T}ime \textbf{S}urgical \\
\textbf{tSNE} \> \textbf{t} distributed \textbf{S}tochastic \textbf{N}eighbour \textbf{E}mbedding \\  
\textbf{USS} \> \textbf{U}ltra \textbf{S}ound \textbf{S}onography \\  


\end{tabbing}

\newpage
\setcounter{page}{1}
\renewcommand{\thepage}{\arabic{page}}
