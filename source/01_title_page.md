<!-- 
This is the Latex-heavy title page. 
-->
        
\begin{titlepage}
    \begin{center}
        
        \vspace*{\fill}
        
        \huge
        Evaluating the utility of gene expression data from patient-matched
        samples for studying breast cancer.        
        
        \vspace{1.5cm}
        
        \Large
        Richard Bownes

        \vspace*{\fill}

        \normalsize
        Doctor of Philosophy
        
        \normalsize
        The University of Edinburgh\\
        2019 

        \vspace*{\fill}

    \end{center}

\pagenumbering{gobble}

\end{titlepage}


