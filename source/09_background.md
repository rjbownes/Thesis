#  Introduction


## Cancer and Therapy 

Cancer represents a state of fundamental dysregulation of the biological and
cellular processes that underpin survival and fitness in healthy cells.  Common
hallmarks of cancer include growth signalling pathways, apoptosis, and
replication[@Hanahan2000]; all of these pathways which represent control over
the cell cycle. In this way, cancer is a corruption of our otherwise normal,
healthy cells to something malignant. However, the dysregulation is present in
our cells, not of a foreign or invasive body. Therefore, cancer therapies treat
the tumour despite the body and must seek to target characteristics inherent to
the process of tumorigenesis, like the significantly higher rates of
proliferation and damaged DNA repair mechanisms [@Cheung-Ong2013]. Anti-mitotic
agents that target the DNA replication mechanisms are effective at targeting
tumour cells but are systemic and non-specific, placing a significant burden on
the patient [@Raguz2008]. Reducing over-treatment and identifying patients who
will have good response to therapy is an important area of research with real
clinical impact [^11] and potential to improve patient care.

[^11]: Due to my unique position between clinical oncology and dry
  lab research I only mean to emphasize the clear target of producing said
  results.


## Breast Cancer - Incidence and Outcomes

Breast cancer is the most prevalent new cancer [^12] among women and the second most
deadly [@Hayes2015; @bray2018global; @Hayes_2015].  Globally, it is the most common cancer in women, with
1.7 million new cases reported each year and over 500,000 deaths in the year
2012 [@Torre2015]. Alone, it accounts for a quarter of all cancer cases
and 15\% of cancer deaths in women [@Torre2015]. Breast cancer represents a
significant number of the total new cases of cancer every year in the UK,
55,200 new cases in 2014 or 15\% of all new cancer cases with a total
mortality of 11,433 [@CRUK1]. Thanks to advances in the treatment
of breast cancer in the last several decades, the total mortality rate has
dropped 32\% since the 1970's and 17\% in the last decade [@CRUK1]. Early
detection of breast cancer can help to increase the survival rates in patients
by significant margins, as much as 20\% after 5 years [@of1988first].
Breast cancer currently has an 89.7\% 5 year-survival rate [@SEER] and a
78\% 10-year survival rate in the UK [@CRUK1] (female cancers only).
Improvements in screening, treatment and diagnosis have produced a positive
trend in the survival rate of breast cancer and are continued fields of
research [@Ferlay15].

[^12]: Specifically malignant cancer, ductal carcinoma in situ is present in an
  overwhelming number of women according to necropsy results but is largely
  asymptomatic.

&nbsp;

In 2012, there were 1.7 million newly
diagnosed cases and 500,000 deaths from BC globally. This represents almost a quarter
of all newly diagnosed cancers, and 15% of deaths from cancer in women
[@Torre_2015]. These numbers represent a heterogeneous disease of different
clinical, histological and intrinsic factors [@Polyak_2011]. In
supervised analysis of patients with known clinical and prognostic outcomes,
and despite tumour cellular heterogeneity,
inter patient variation often exceeds the variance between groups of patients
with regard to biological or clinical status [@Stephens_2012]. This means that identifying trends that significantly
differentiate translational changes between responsive and non-responsive
breast cancer can be very difficult.


## Detection, Diagnosis and Subtypes

\FloatBarrier

Breast cancer is frequently detected by screening in countries where these
programs are prevalent. Approximately two million women a year have a breast
screening and all women between the ages of 50 and 70 are invited for screenings
every three years [@CRUKScreening]. These screenings are responsible for reducing
mortality in the UK by as many as 1300 women per year. Additionally, masses
can be detected by a physician, usually following a clinical breast examination
as a result of patient reported symptoms [@CRUKScreening; @Saslow]. A lump
can be ratified by mammogram, MRI, and ultrasound but a definitive diagnosis
can only be given through biopsy. Biopsies are performed as either a fine needle
aspiration biopsy, a core needle biopsy, or as a surgical biopsy and the sample
is examined by a pathologist in order to determine the malignant or benign
nature of the mass. The pathological report contains key information about the
tumour that correlates with long term survival information [@Dackus]. The
clinical and pathological information is crucial for both the patient and
clinician decision making and a better understanding of tumour molecular and
histological sub-typing will aid in the prognosis and treatment
[@malhotra2010histological].

&nbsp;

Breast cancer is normally defined as either *in situ* (in its original place) or
invasive; both are further dichotomized into more refined subtypes,
usually ductal or lobular [@malhotra2010histological]. BC is broadly defined as
a malignancy originating in breast tissue of men or women [@Cancer.org] and is
frequently described by the structure from which it originates either ductal or
lobular or as one of a few rarer sub-categories of breast cancer including Paget's,
Phyllodes or inflammatory breast cancer [@Breastcancer.org]. Ductal Carcinoma
*in situ* (DCIS) is the most common *in situ* subtype of breast cancer and is
further described as either comedo or non-comedo; non-comedo tumours are
classified as cribiform, micropapillary, papillary, and solid
[@malhotra2010histological; @connolly]. Invasive carcinomas are those that
have infiltrated past their point of origin and are also subdivided into
different groups based on their histological features, the most common being
Invasive Ductal Carcinoma (IDC) of no special type, which makes up between 70
and 80\% of all invasive breast cancer tumours [@li2005clinical]. All tumours
can be further subdivided by the tumour grade as a function of its differentiation;
grade 1 (well differentiated), grade 2 (moderatley), and grade 3 (poorly)
[@li2005clinical]. Other common categories of invasive carcinoma include
lobular, ductal/lobular, mucinous, medullary, papillary and tubular
[@malhotra2010histological]. See Figure \ref{fig:ia} on Page \pageref{fig:ia} for an illustration of the clinical
subtypes of breast cancer.

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Downloads/Histological_BC.pdf}
    \caption{\textbf{Histological Subtypes of Breast Cancer.} An illustration of the broad categories of histological breast cancer subtypes.}
    \label{fig:ia}
\end{figure}

\FloatBarrier


## Treatment Options and Decision-making

\FloatBarrier


The most common and conventional means of treating breast cancer are radiotherapy,
chemotherapy, hormone therapy and targeted therapies [@Dhankhar2010].
Conservation of breast tissue in the treatment of localized disease, rather
than masectomy, is now the most common surgical approach to breast cancer
[@Matsen2013; @board2017breast]. When surgery is preceded by treatment
(neoadjuvant setting), it may shrink the tumor volume and reduce the amount
of breast tissue resected [@miller2014current]. Further treatment is
usually given after surgery (adjuvant setting) in order to reduce the risk of
recurrence or metastasis and improve the patient outcomes
[@miller2014current; @Dhankhar2010]. Choosing a therapeutic pathway should be made with the all of the information available
on the tumour, patient characteristics, pathological features, and histological information, such as the
patients risk of recurrence and metastasis [@Nounou2015]. Additionally, the
inclusion of intrinsic phenotypes (ER/PR ,HER-2 and Ki-67 status derived)
should be considered in the treatment decision making process where possible as
these factors have been shown to add prognostic value to clinicians[@Goldhirsch2013]. 

&nbsp;

Non-specific treatments for breast cancer are radiation therapy and
chemotherapy. These treatment options do not discriminate between cell types
but are designed to take advantage of cancer's proliferative nature. Radiation
therapy is the direct exposure of the cancer cells to radiation and is
frequently given in combination with chemotherapy, as it can be used to further
shrink the tumour bulk and reduce the chance of recurrence [@Akram2012].
Chemotherapy is usually given to patients presenting as ER-, especially triple
negative tumours, however it is also prescribed to HER+ patients as well as
Luminal tumours (ER+) at higher risk [@Nounou2015]. There are a few options
in the treatment of cancer with chemotherapy, historically CMF, but more
recently taxanes and anthracyclines. Taxanes and anthracyclines may be given
independently or in combination and have been shown to have improved
performance over treatment with CMF [@Shao2012]. The combination therapy of
taxanes and anthracyclines was shown to reduce mortality rates by one third,
regardless of patient age or tumour characteristics
[@EarlyBreastCancerTrialistsCollaborativeGroupEBCTCG2012]. However, these
compounds are not generally well tolerated and can have adverse toxic effects,
especially to those with preexisting co-morbidities and heart problems
[@Valero1997].

&nbsp;

The purpose of endocrine (hormone, ET) therapy is to block the
activity of hormones and is recommended to all patients with significant
endocrine receptor expression (defined as >1% of the cells)
[@Dhankhar2010]. ET can be used in addition to chemotherapy, radiation and
targeted therapies, as long as the patients are ER+[@Dhankhar2010]. The choice of treatment,
however, is dictated by the patient's menopausal status [@Dhankhar2010]. For
pre-menopausal patients, treatment with an ER antagonist is standard (e.g.
tamoxifen, 5-10 years) and can be combined with ovarian ablation (surgically or
medically) but comes with associated risks to fertility and endometrial
hyperplasia and cancer [@Hirsimaki]. When combined with ovarian ablation it
is at least as effective as treatment with CMF [@Puhalla2009] and
the combination of an AI with ovarian ablation or administration of a gonadotropin
releasing hormone (GnRH) agonist is a tolerable substitute for tamoxifen
[@Dowsett1992]. In post-menopausal women the ovaries are no longer
producing oestrogen, therefore ablation or GnRH-agonists are no longer necessary
and the standard treatment for this cohort then becomes 5-10 years of adjuvant tamoxifen
[@Davies2013].

&nbsp;

Another prominent targeted therapy is trastuzumab, which has been shown to
reduce the recurrence of HER2+ breast cancer in combination with chemotherapy
by half when compared to chemotherapy alone[@Callahan2011]. Trastuzumab in
combination with chemotherapy and another HER2+ agent saw improved treatment
response[@Callahan2011; @Tsang2012]. Trastuzumab has been approved for use
in patients with positive nodal involvement or larger tumours (greater than 2 cm
diameter) and for concurrent use with with taxanes, however concurrent administration
with anthracyclines is ill advised due to both therapies having notable cardiotoxicity
[@OSullivan2015; @Ewer2008; @Turner2015]. Additional targets for targeted
therapies include PI3K/mTOR and CDK, as these are frequently dysregulated in
breast cancer [@Vinayak2013; @Mayer2015]. Everolimus (an mTOR inhibitor) and
Palbociclib (a CDK inhibitor) are FDA approved drugs with improved treatment response in
combination with existing chemotherapy agents in a variety of breast cancer types. Breast cancer treatment
strategies are summarised in Figure \ref{fig:ii} on Page \pageref{fig:ii}. 

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Downloads/newfig2.pdf}
    \caption{\textbf{Generalized treatment strategies for breast cancer.} A
    basic overview of the treatment options for different clinically relevant
    subtypes of BC are shown in this diagram to illustrate the heterogeneous
    nature of BC treatment.} \label{fig:ii}
\end{figure}

\FloatBarrier



## Clinical Decision Making Tools

There are applied decision making tools currently in use that combine multiple
clinical factors of the patient and tumour to help make informed treatment
choices and prognosis. The Nottingham Prognostic Index combines factors
including the number of involved lymph nodes, tumour size, and tumour grade to calculate a score that is
descriptive of prognosis in an attempt to identify high risk, poor prognosis
patients [@Haybittle1982]. The NPI has proven to be more informative than
lymph node status alone as a means for predicting prognosis [@Haybittle1982].
The Nottingham Prognostic Index has also been validated in other British datasets and treatment
centers [@Wishart2010]. Adjuvant! Online is another clinical tool that
attempts to predict and quantify patient response to either endocrine or
chemotherapy treatment in terms of predicted overall survival rates and
recurrence free survival [@DeGlas2014]. These estimates are derived from
patient age, menopausal status, tumour staging, number of involved nodes, and ER
status [@DeGlas2014]. PREDICT is the first prognostic tool that also
encompasses mode of detection when calculating patient outcomes and has been
proven to be an effective and discriminatory means of prognostication
[@Wishart2010]. In this regard, PREDICT performs well across different
prognostic groups and age ranges and builds on work like Adjuvant! Online in
to create new and contemporary prognostic tools [@Wishart2010].


## Molecular Subtyping and Prognostic Signatures in Breast Cancer

\FloatBarrier

In addition to histological subtypes, breast cancer tumours can be described by
receptor status and, more recently, by HER2 and TNBC labels. There are
experimental molecular subtypes based on gene expression that have been
popularized by Perou and Sorlie [@sorlie2003repeated]. These intrinsic subtypes
of breast cancer include Luminal A (High ER, low Her2) and Luminal B (Low ER,
Low Her2 and High Proliferation), Her2 (Her2 positive, ER negative), Basal (ER
negative, Her2 negative, PR negative) and normal like [@perou2000molecular;
@sorlie2001gene; @sorlie2003repeated]. Molecular subtypes may be of use for
predicting response to specific therapies [@malhotra2010histological], and at
least in the case of invasive carcinomas, it can be recommended to calculate the
relevant sub-typing information for prognostic consideration
[@harris2007american]. The generation of these subtypes may one day have
widespread clinical applications for patient and clinician decision making
because they have significantly different survival characteristics, for example, Basal
like tumours having the poorest prognosis [@sorlie2001gene]. Due to prohibitive
costs, these methods have not seen broad clinical uptake but the creation of
targeted gene panels, like PAM50, have made the generation of molecular subtypes
possible on a small scale, and have been shown to be comparable to full
microarray analysis [@parker2009supervised].  Methods like PAM50 have also been
shown to out perform clinical information (T, N, M and grade) in isolation for
predicting recurrence but in together show improved predictive
power and a potential future opportunity for research [@parker2009supervised].

&nbsp;

Seminal gene expression profiling studies by Sorlie and Perou first described the
so called "intrinsic" molecular subtypes of breast cancer that correlate with
distinct clinical applications in terms of treatment and prognosis
[@Sorlie2001; @Perou2000]. Luminal tumours, A and B, have positive
hormonal receptor status and have expression profiles similar to normal luminal
epithelial cells of breast tissue but differ more than "normal" type
tumours [@Perou2000]. Immunohistochemically, they appear as ER+/PR+/HER-
(Luminal A) and ER+/PR+/HER+/- (Luminal B) [@Vallejos2010] but Luminal A
have lower levels of expression for proliferation and are usually a lower grade
than Luminal B [@Sorlie2001; @Dai2015]. Luminal subtypes of breast
cancer are the most common [@Dai2015] but have distinctly different
prognoses; Luminal A usually exhibits a much better treatment outcome than
Luminal B[@Sorlie2003]. With the two ER- tumours, these make up the four distinct
intrinsic molecular subtypes: Luminal A, Luminal B, HER2-enriched and
basal-like [@Perou2000]. These subtypes are well utilised in research,
however they have seen little clinical application and there is not yet a consensus
view of their experimental value. The fifth subtype, normal like, is now believed to be
caused by contamination of normal mammary cells in the tumour biopsy
[@Hon2016]. Separating patients into these subtypes has important clinical
ramifications, as they have variable treatment options and different prognoses.
For example, patients with Luminal or HER2 subtypes have options for targeted
and hormonal therapies, while patients with basal subtyped tumours gain no
advantage from these treatments and only have chemotherapeutic options
[@Hon2016]. Prognostic signatures, like PAM50 and others, have been
developed to give patients and clinicians improved treatment options
[@Hon2016].

&nbsp;

HER2-enriched tumours are those with amplification/over-expression of HER2 and,
which under IHC classification, appear as ER-/PR-/HER2+ [@Vallejos2010].
HER2-enriched tumours frequently overexpress related genes to HER2, namely
GRB7 and PGAP3 and are most likely grade 3 [@Dai2015]. HER2-enriched
tumours often have poor prognoses [@Sorlie2001] but have good rates of
pathological clinical response due to the availability of targeted agents and
susceptibility to standard chemotherapies [@Brenton2005]. Basal tumours, or
triple negative tumours, are characterized under IHC as ER-/PR-/HER2- and have
expression profiles similar to basal epithelial cells of breast tissue
[@Perou2000]. Due to the lack of receptor targets, the only standard of care
is chemotherapy which in conjunction with the generally aggressive nature of these
tumours leads to basal-like tumours having the worst clinical prognosis of the
molecular subtypes [@Dai2015; @BCDiagram]. The poor prognosis is also partly explained
by the lack of treatment options available and recurrence rather than a lack
of treatment efficacy [@Brenton2005]. A summary of the characteristics of
the molecular subtypes is included in Figure \ref{fig:iii} on Page \pageref{fig:iii}. While oestrogen
receptor negative tumours tend to be of higher grade and have a worse prognosis
than ER+ counterparts, a small subset of triple negative breast cancers have
better clinical outcomes and respond well to neoadjuvant chemotherapy, showing
much better overall survival [@Teschendorff2007; @Lehmann2016].
Further analysis has shown that TNBC is a heterogeneous disease with between
four and six potential subtypes, each characterised by distinct patterns of
gene expression that have different treatment and survival characteristics
[@Teschendorff2007; @Lehmann2016; @Lehmann2011]. This subsetting
of TNBC may be important for future drug discovery and biomarker generation for
these difficult to treat tumours [@Lehmann2011].

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Thesis/source/figures/replacement.png}
    \caption{\textbf{Breast cancer molecular subtypes, prognosis, receptor
    status and treatment options.} This diagram shows the
    molecular subtypes of breast cancer and how their receptor status relates to their relative prognosis.}
    \label{fig:iii}
\end{figure}

\FloatBarrier

## Publicly Available Gene Expression Datasets 

\FloatBarrier

Newer and larger sources of publicly available transcriptomic data have enabled breakthroughs
in "Precision Medicine" and will be crucial to the future improvements of
the treatment and prevention of breast cancer [@Clare2016]. The predominant sources
of these datasets are, by large, initiatives and national projects including the
Cancer Genome Atlas (TCGA) and the International Cancer Genome Consortium
(ICGC). Large efforts, like these, to collect and make available databases
facilitate new waves of *in silico* hypothesis testing to researchers without the ability to develop their own large-scale cohorts.
This data is fundamental to improving the scientific process of individual and
personalized medicine, as well as powerfully proving or disproving currently
held assumptions [@Clare2016]. 

&nbsp;

There have been several recent seminal database publications with far reaching
impact and utility. The Molecular Taxonomy of Breast Cancer International
Consortium (METABRIC) is one such effort that sequenced 173 genes in 2433
patients, along side expression data, copy number aberrations (CNA), and long
term follow up [@Pereira2016]. This study identified 40 driver mutation
genes and correlations between the measured genomic information and survival to
assess the importance of genome based analysis and stratification of breast
cancer subtypes for therapeutic purposes [@Pereira2016]. The dataset was made
publicly available in 2012, with sequencing data added later, and led to the
development of the IntClust (IC10) novel sub-grouping classifier based off
upstream driver genes identified by the integration of genomic and transcriptomic data
[@Ali2014].

&nbsp;

The Cancer Genome Atlas collected DNA copy number, methylation information,
array and sequence based mRNA expression, micro RNA and protein/phosphoprotein
expression information for 825 primary breast cancer patients
[@Koboldt2012; @Clare2016]. They identified specific and significant
mutations in TP53, PIK3CA and GATA3 across all breast cancers and found
evidence for four primary breast cancer subtypes, each distinct and with
significant molecular heterogeneity [@Koboldt2012]. Comparisons were also
drawn between basal like breast cancer and high serous grade ovarian cancers
with regard to molecular commonalities and they hypothesized the opportunity
for shared therapeutic options [@Koboldt2012]. Additionally, they concluded
that clinically observed heterogeneity in breast cancer was mostly within and
not between distinct breast cancer subtypes [@Koboldt2012]. 

&nbsp;

Outside of major studies online repositories exist to host individual datasets
from researchers and clinicians including the NCBI, the European Bioinformatics
Institute and, at a smaller scale, The Swiss Institute of Bioinformatics. These
accepted deposited data and host datasets of genomic information and allow for improved access
to the available public data. Large scale changes and additions to these
repositories can be found in the *Nucleic Acids Research* (NAR) annual
database overview. These additions show the rate of growth of available
genomic information and highlight the increase in availability of such data.
Figure \ref{fig:NAR}, Page \pageref{fig:NAR} shows the rate of additions and
updates to the NAR over a four year period[@Galperin2017; @Rigden2016;
@Galperin2015; @Fernandez-Suarez2014]. While these represent different types of
datasets, proteomic, transcriptomic, and genomic, the rate at which new datasets are
accepted shows the trend in the availability of data. The available amount of
information for bioinformatic analysis is growing yearly and is opening new
pathways to treatment and therapy. Other large scale dataset projects have
taken the open source approach, creating useful, open access tools for
generating cancer signatures in breast cancer and in other tumours [@gendoo2019metagxdata; @TCGAdata].

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Thesis/source/figures/tabel_data.png}
    \caption{\textbf{NAR year on year growth.} The 2017
    publication of the NAR database issue saw the addition of 54 new data bases
    and 98 updates to existing data bases, a steady increase from 2016 which
    saw 62 new databases and 95 updates, and 2015/2014 which saw 56 and 115,
    and 58 and 123 new databases and updates respectively} 
    \label{fig:NAR}
\end{figure}


\FloatBarrier

## Gene Expression Profiling Methods and Datasets

Gene expression profiling gives a snapshot of a tumour's cellular activity as a
function of the level of activity of all measured genes at a specific point in
time. This has enabled the detection of differences in gene expression levels
among different types of breast cancer and helped to create subtype specific profiles
[@VantVeer2005]. This technology has helped to better understand the
heterogeneity of breast cancer and is offering new clinical tools for the
prognosis and treatment of breast cancer [@Bao2008]. Some profiling
techniques OncotypeDX (Genomic Health Inc, Redwood City, CA) and MammaPrint
(Agendia, Amsterdam, The Netherlands) are already showing validated utility in
this area, are commercially available, and are being refined through ongoing
clinical trials [@Bao2008]. This progress is aided by the development of
newer and more efficient means of sequencing large scale genomic information.
Microarray platforms for generating the necessary expression level data, like
Affymetrix, use thousands of nucleic acid probes to hybridize complementary
nucleic acid sequences in solution and measure their relative concentrations to
calculate gene expression levels estimated indirectly by observing the
hybridization events [@Bumgarner2013]. Beadchip sequencing techniques, like
Illumina, use thousands of 3 micron silicon beads dispersed in random wells on
a testing substrate. These wells are covered in hundreds of thousands of
oligonucleotide primers and the concentration of binding at each bead is measured
with fluorescence to estimate the amount of the gene bound
[@Illumina]. Lastly, RNAseq utilizes deep-sequencing and isolates sample
RNA, generates complementary cDNA and sequences the fragments before aligning
the reads in order to map the sample transcriptome. RNAseq has single base
resolution and is sensitive enough to distinguish isoforms, but is the most
costly of these technologies [@Wang2009]. These techniques have all helped
to resolve detail in expression level data from patient samples and have made
the predictive and prognostic tools that are available possible. 


## Prognostic Gene Expression Signatures for Breast Cancer

The most established gene expression based prognostic tools for breast cancer
are Oncotype DX and MammaPrint. Oncotype DX is a genomic assay (Genomic Health,
Inc., Redwood City, CA, USA) used to predict the likelihood of breast cancer
recurrence and aids in the treatment selection decision making process for
patients with ER+ disease [@McVeigh2017]. Oncotype DX works by
comparing the expression of 16 key genes to the expression of 5 references
genes and algorithmically generates a recurrence score based off these
measurements [@Cronin2007]. The effectiveness of Oncotype DX has been
independently validated in node-negative patients to accurately predict risk of
recurrence regardless of patient age or tumour size [@Cronin2007]. Additionally, the National Surgical Adjuvant Breast and Bowel Project (NSABP)
B-20 trial showed that the calculated recurrence scores could be used to
identify the patients to whom adjuvant chemotherapy conveyed the most
advantages over endocrine therapy alone. It found that patients with high risk
of relapse gained the most added benefit from adjuvant chemo (RS scores
greater than or equal to 30%) [@Paik2006]. MammaPrint is a microarray-based, recurrence risk
test reliant on the expression of 70 genes that make up its specific signature
[@Beumer2016]. This test functions as a means of identifying patients with
a low or high risk of distant metastasis in order to aid physician treatment
selection and minimize unnecessary treatment, as patients labelled as low risk
can omit chemotherapy without a reduction in disease-free survival
[@Cardoso2016]. This result was ratified in the RASTER trial, showing that
in the 5-year, distant recurrence-free interval (DRFI), the use of MammaPrint
correctly identified low risk patients and the omission of additional
chemotherapy did not compromise their outcome [@Drukker2013]. 

&nbsp;

While both MammaPrint and OncotypeDX methods identified patient prognosis and help identify patients who do
not need additional chemotherapy, there are a few key differences between the
techniques. Both can now be applied to FFPE samples, though, initially, MammaPrint
was more limited in application, as it could only be performed on fresh frozen
samples [@Gyorffy2015]. Additionally, while both are primarily for ER+ tumours, MammaPrint was developed with a younger cohort and is most
appropriately applied to patients under 61 years of age [@Gyorffy2015].
Finally, trials for both methods are underway to identify if either is
appropriate for ER- tumours, which would differentiate them
substantially and increase their utility in the future. Other techniques for
gene expression profiling that are less well adopted include the Genomic Grade
Index and PAM50 (Prosigna). 

&nbsp;

PAM50 (Prosigna; Nanostring Technologies, Seattle,
WA, USA) is another recurrence scoring test for ER+ patients designed
off 50 key genes and is appropriate for postmenopausal women undergoing
endocrine therapy [@Gyorffy2015]. PAM50 can report the intrinsic subtype of the
tumour [@Dowsett2013] and has the added advantage of being performable locally
[@Gyorffy2015], whereas MammaPrint and Oncotype DX must be performed at
specified testing centres. The Genomic Grade Index (MapQuant Dx,
Ipsogen, France) represents an entirely different strategy, which is currently used to
better define histological grade assessments [@Filho2011]. Instead of
assigning grades to tumours, it defines low and high molecular grade risk
groups based on a 97 gene signature (or abbreviated 6 genes RT-PCR signature)
in order to separate tumours into more distinct prognosis groups
[@Filho2011; @Gyorffy2015].

&nbsp;

EndoPredict is a validated assay tool used in the prediction of metastasis for
patients with ER+, HER2- breast cancer [@Muller2013]. EndoPredict
incorporates both clinical and genomic information to calculate
the risk of metastasis on a per patient basis[@Muller2013]. This information can be used to
inform treatment options and has already been used to advise treatment
progression on patients in the study cohort, resulting in additional
chemotherapy for some high risk patients or endocrine treatment only for lower
risk individuals [@Muller2013]. The Breast Cancer Index is a gene
expression based signature for predicting the early and late life-time disease
recurrence of breast cancer in ER+ patients [@Zhang2013]. This test was
shown to perform well from the time of diagnosis for the prediction of high and
low risk disease recurrence[@Zhang2013]. This information can be used to help inform treatment
decisions as well as the suitability of long term adjuvant endocrine therapy
[@Zhang2013]. Newer tests are being developed for specific treatment
cohorts that have refined applicability and improved resolution of patient
prognosis, which can potentially provide significant improvements in the treatment
decision making process. One such test is the OncoMasTR, which used network
delineation to identify the underpinning genes that are the upstream drivers of
breast cancer in a cohort of ER+ LN- patients [@Lanigan2015]. The seven
genes derived from this transcriptional regression were shown to be more
sensitive when predicting the need for additional chemotherapy for women with
no lymph node involvement[@Lanigan2015]. OncoMasTR can be used in the future to assist in treatment
decision making for similar patients in an effort to avoid unnecessary
chemotherapy [@Lanigan2015].


## Sequential Sampling

\FloatBarrier

Sequential sampling, taking several samples over time to monitor the change in
tumour biology, is offering new insights into the effects of treatment on
breast cancer, see Figure \ref{fig:iv} on Page \pageref{fig:iv} for an illustration of the approach. Initially uncommon, the
collection of multiple biopsies from the same patient has become more routine
with the rise in neoadjuvant therapies [@Untch2014]. Studies of neoadjuvant
therapies and pre-surgical treatments allow for unique *in vivo*
analysis of tumour treatment response [@Macaskill2007], as well as the
possibility of predicting the response to treatment earlier in the treatment
cycle [@Sims2008].

&nbsp;

Neoadjuvant studies of sequential samples take place after diagnosis but
before surgery, providing a "window of opportunity" that offers the possibility
of observing translational changes that are solely the tumour's response to
treatment [@Pearce2016]. Measuring these changes can potentially indicate
if a tumour is likely to respond to treatment or generate resistance and both
outcomes can help inform patients and clinicians on how to proceed with
treatment [@urruticoechea2005proliferation; @makris1997prediction; @faneyte2003breast]. Though the exact structure of sequential studies will differ,
diagnostic biopsy samples as well as one or more on treatment samples and the
surgical/resection sample are usually collected. Acquisition of these
samples can prove difficult for patient retention in trials, especially in the
CT setting, where treatment side effects are much more pronounced and
additional patient stress is a contributing factor.

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Thesis/source/figures/Picture1.jpg}
    \caption{\textbf{Window of Opportunity Schematic for Neoadjuvant Therapy.} The
    general structure of Neoadjuvant trials and treatment strategy are illustrated here,
    with a pre-treatment opportunity to sample the tumour positive tissue and
    measure the effects of therapeutics prior to surgery.}
    \label{fig:iv}
\end{figure}

\FloatBarrier

## Opportunities and Challenges of Patient-matched Samples

Sequential samples offer increased resolution for the change in tumour biology
with treatment. Most studies focus solely on the presentation of the tumour at
diagnosis [@Sims2008], however by looking at the on treatment changes,
important pathway expression can be observed that are linked to different
clinical outcomes [@Gee2005]. Additionally, these sequential samples can
offer insight into the molecular changes that drive tumour evolution in
patients. Specifically, Guttierez *et al*. [@Gutierrez2005] recently reported
significant HER2 and MAPK signalling in patients with acquired resistance to
tamoxifen in matched pairs of breast samples.

&nbsp;

Sequential sampling offers a unique opportunity to study drug efficacy,
predict response, and provides an increased rate of predictive biomarker generation
[@Pearce2016]. This has been demonstrated already by research in Edinburgh
through the addition of on-treatment expression information to enhance
predictive power, which resulted in a four-gene biomarker that is 91\% accurate
in validation and significantly predicted both recurrence and breast
cancer-specific survival rates [@Turnbull2015]. More work is needed in the area
to demonstrate if this only holds true for ER+ cohorts or if this is more
generally applicable. Challenges involved in collecting sequential samples are varied,
additional sampling is inherently more expensive, and patients are subjected to
additional stress. Early on-treatment biomarkers for response would reduce the
incidence of mis-treatment and remove the burden of multiple biopsies. Further
more, Turnbull *et al.* have shown that a two-protein signature can be highly effective
for predicting response in patients at early time points [@SABCSposter]. 

## Predictive Biomarkers for Patient Response

Biomarkers for cancer fall into three categories; predictive, prognostic or
diagnostic [@Goossens2015]. Predictive biomarkers are utilized to predict
response to specific therapies like trastuzumab, where the activation of HER2
pathways in breast cancer is indicative of response [@Goossens2015]. Prognostic biomarkers are not treatment specific, instead
they attempt to quantify the risk of future relapse or recurrence of cancer to
a patient and help clinicians make informed treatment decisions
[@Goossens2015]. Lastly, diagnostic biomarkers are used to ascertain if a
patient has a specific form of a disease [@Goossens2015]. As has been
touched on, most biomarkers and prognostic tools are developed using single
time point patient and tumour information and do not take into account any
on-treatment information. The addition of on-treatment information may be
advantageous for the development of robust new biomarkers to aid clinicians. 


## Availability of Datasets

\FloatBarrier

Studies of sequential time points like those discussed previously are rare
compared to single time point studies. Table \ref{tab:tbli} on Page \pageref{tab:tbli} highlights some of the available datasets of sequentially
sampled patient
studies [@Turnbull2015; @Miller2009; @Dunbier2013; @Morrogh2012; @Brandao2013; @Mackay2007; @Sabine2010; @Korde2010; @Tempfer2011; @Hannemann2005; @Pearce2016].
It is worth noting the discrepancy between endocrine and chemotherapy in terms
of patient cohort size; endocrine studies have a higher success rate for
sequential biopsies because of the reduced levels of stress experienced by
patients compared to chemotherapy. Practically, this means more chemotherapy trials
of sequential samples must be aggregated in order to reach similar levels of
significance for any subsequent results. Not all
datasets are annotated well, some lack phenotypic data for each patient or
defined response status (if available) in different ways, i.e.
radiological vs pathological complete response. Therefore, validation
across these disparate datasets becomes more challenging as common end points
needs to be defined to draw direct comparisons. They are also processed across
different platforms, leading to the inclusion of batch effects that need to be
considered and removed [@Chen2011] in order to adjust for technical variance while
retaining the underlying biological information [@Muller2016]. Lastly, while sequential in nature, time points can differ between
studies. All studies have a pre-treatment biopsy sample but the first on-treatment
sample is not always uniform nor are subsequent time points. Lastly, datasets may contain a variety of different on-treatment time point
samples due to different trial and study structures. Finding a lowest common denominator of treatment samples is
necessary for direct comparisons but will inevitably reduce the available
samples. 


\begin{landscape}

\begin{table}[htp]

\centering

\resizebox{\textwidth}{!}{%

\begin{tabular}{|l|l|l|l|l|}

\hline

Treatment Type & Days on Treatment & Number of samples & Platform & Date and Study \\ \hline

Letrozole & 0,14,90 & 89,89,89 & Affymetrix U133A and Illumina HT-12 & \begin{tabular}[c]{@{}l@{}}Turnbull et al. 2015\\ (Miller et al. 2009)\end{tabular} \\ \hline

Anastrazole & 0,14,90 & 112,97,29 & Illumina WG-6 & Dunbier et al. 2013 \\ \hline

Anastrazole & 0,9-63 & 26,26 & Illumina DASL & Morrogh et al. 2012 \\ \hline

Celecoxib & 0,14-21 & 22,22 & Affymetrix U133 Plus 2.0 & Brandao et al. 2013 \\ \hline

\begin{tabular}[c]{@{}l@{}}Randomised Letrozole \\ \\ vs. \\ \\ Anastrozole\end{tabular} & 0,14 & 34,34 & Custom 17K & Mackay et al. 2007 \\ \hline

Everolimus & 0,14 & 23,21 & Illumina Ref-8v2 & Sabine et al. 2010 \\ \hline

Docetaxal and capecitabine & 0,21 & 21,14 & Affymetrix HG-U113 Plus 2.0 & Korde et al. 2010 \\ \hline

Epirubicin or cyclophosamide & 0,84 & 32,25 & Agilent 44K & Stickeler et al. 2011 \\ \hline

\begin{tabular}[c]{@{}l@{}}Doxorubicin/cyclophosamide \\ \\ or \\ \\ doxorubicin/docetaxal\end{tabular} & 0,126 & 31,17 & 18K NKI microarrays & Hannemann et al. 2005 \\ \hline

No-Intervening Treatment & 0,13-53 & 37,37 & Illumina HT-12 v3/4 & Pearce et al. 2016 \\ \hline

Anthracyclines and Taxanes & 0,14,42,90 & 34,12,23,24 & Ampliseq & Bownes et al 2019 \\ \hline

Anthracyclines followed by Taxanes & 0,1-4,90 & 221,36,32 & Custom cDNA & I-SPY Trial \\ \hline

Anastrazole, Letrozole, Exemestane & 0,14,28 & 58,58,60 & Agilent Array & Ellis et al. 2017 \\ \hline

\end{tabular}%

}

\caption{\textbf{Descriptive Table of Some Available Sequential Breast Cancer
DataSets.} Many of the available datasets for matched pre and on-treatment primary breast cancer are quite
small. This means the individual results of analysis of these data are
potentially lacking in power or significance.}

\label{tab:tbli}

\end{table}

\end{landscape}

\FloatBarrier

## Dataset Integration

A significant problem for sequentially sampled dataset analysis is the size
and availability of data. There are multiple obstacles that prevent the
easy integration of data to improve sample size, primarily, batch effects and the influence of
different processing platforms or methods. However, when discussing the impact on
time-dependant data, on treatment effects become a problem. Contemporary work
ongoing at the University of Edinburgh is showing that integration of
compositionally diverse data sets can skew subtyping results. Additionally, integrating treated and untreated data together can reduce distinctions between
the sample populations. All of these factors will have to be combated but with
successful integration of sequential datasets the ability to gauge the effect
of treatment and leverage the pairwise patient response for risk stratification
is improved.

&nbsp;

Dataset integration is an attractive target for furthering bioinformatics, as
it is desirable to create larger, higher value datasets. These can improve the
statistical significance of results and enhance detection of low frequency anomalies from
heterogeneous data. However, integration can also introduce bias and statistical
artefacts due to the nature of batch correction methodologies seeking to
normalize parametric distributions. These are clearly identified
through visualizations of the data and the effects of integration through
techniques like principal component analysis. Small and disparate datasets are
a significant bottleneck to generating novel biomarkers and prognostic
signatures [@Saini2014]. As there no standard for processing platforms exists, the need for a robust
integration method becomes more evident as more high quality genomic data is
made available [@Shen2004]. As expression data can
vary greatly between platforms in terms of scale and structure, integrating,
comparing and validating results across independent studies remains challenging
[@Shen2004]. Batch effects can confound the integration of disparate datasets
and removing systematic differences between datasets is necessary for the
robust integration of data. As demonstrated by Sims *et al.*,  removing batch effects and
reconciling systematic biases between expression sets allows for the direct
integration of raw expression data with improved statistical significance for
downstream analysis [@Sims2008].

## Dataset Integration Improves Statistical Significance

Dataset integration allows for the creation of larger and more representative
cohorts for statistical analysis and would eliminate the effects of overfitting on a resultant biomarker, due to the wide nature of gene expression
data. The small size and specificity of many modern datasets give rise to
incomplete clinical tools that have difficulties validating in exogenous data
[@Teschendorff2006; @Simon2005]. Currently, several prognostic and
profiling signatures have been shown to have some level of concordance in
independent datasets [@Teschendorff2006], however it has been established
that much larger sample sets are necessary to make true consensus biomarkers
[@Ein-Dor2006]. Additionally, many prognostic markers and profiling
signatures do not validate in other datasets, even when the differences
between the sets are minimised[@Naderi2007]. This is frequently due to
overtraining of prognostic markers on small datasets that inhibit their general
utility in disparate/dissimilar datasets. 

&nbsp;

There are a number of technical difficulties that plague genomic data
integration. Due to differences in pre-processing pipelines and gene and probe-level
annotation feature size of integrated data is often diminished in order to
retain complete feature coverage[@Hamid2009]. Several groups have reported that integration of
expression microarray data has positive effects on statistical power, the ability
to identify differentially expressed genes of interest, and more robust,
reproducible results [@Hamid2009; @Choi2003; @Shabalin2008]. While there is no
standard for the method of integration, it has been shown
that as long as the integration methodology is rational and carefully executed,
the improvements to downstream analysis are significant [@Hamid2009].

&nbsp;

There are few existing examples for the integration of sequential samples.
Primarily, this may be due to the relative scarcity of sequential data sets and
the added confounding factor of patient and treatment response heterogeneity.
Integration must be carefully considered, as it has been shown previously that
dataset composition can affect the accuracy of expression derived subtypes in
breast cancer [@Paquet2015]. As on going treatment changes the predicted
intrinsic subtype, integration of different time points must be handled
carefully to avoid normalizations across treatments that could potentially distort
gene expression measurements and introduce, rather than remove bias.

## Using On-Treatment Information May Enhance Prediction of Response or Prognosis

There is a substantial precedent for the clinical use of gene expression profiling. This is primarily based off correlated relationships between
pre-treatment patient samples, post treatment patient samples, and long term
survival analysis. While this methodology has proven suitable in the generation
of successful expression signatures for prognosis prediction (Onxotype DX,
MammaPrint etc.), the scarcity of on treatment information has made the
generation of signatures from sequentially sampled data significantly more
rare. Developing early on treatment profiling signatures for response in both
neoadjuvantly administered chemotherapy and endocrine therapy could improve
patient care and reduce over prescription. As breast cancer is a multifaceted
disease, a single biomarker is unlikely to be successful at predicting response
in a molecularly diverse setting. Fortunately, there is now access to
sequentially sampled data for both treatments types, which facilitates the generation
of niche markers for both treatment paradigms.

&nbsp;

A new landmark study in this field, the POETIC trial, is a phase III,
multicentre, randomised trial of ER/PR+ breast cancer measuring the effect of perioperative
aromatase inhibitor with on-treatment changes to KI-67[@dowsett2011endocrine;
@bliss2011ot2; @smith2011trial;]. This study builds on the establish results of
the IMPACT trial which suggests that on-treatment changes in KI-67 at two weeks
are strongly suggestive of patient outcome [@smith2005neoadjuvant]. Ellis *et
al.* has also demonstrated that on-treatment information can be
informative for the accurate prediction of response to endocrine
therapy[@Ellis2017]. In a 2017 study they examined the expression levels of KI-67 in patients
at two and four weeks where they found that patients with elevated
Ki67 levels (higher than 10\%) were exhibiting endocrine
resistance and were triaged to neo-adjuvant chemotherapy [@Ellis2017]. This
cohort experienced lower rates of pCR (5.7\%, 2/35 patients) [@Ellis2017].
Individually these all strongly support the use of on-treatment gene expression
monitoring for improved patient stratification.

&nbsp;

Studies of integrated pre and on treatment information are still in their
infancy. However, Turnbull *et al.* revealed that on-treatment proliferation
markers could be combined with patient-matched pre-treatment markers to
accurately predict the clinical response and recurrence-free survival of
patients[@Turnbull2015]. In this study, the tests utilising on-treatment
information outperformed pre-treatment only diagnostic tools, in both accuracy
and specificity[@Turnbull2015]. The on-treatment signature developed was found to be 96\%
accurate, while the pre-treatment only signature was 93\% accurate in a local
dataset [@Turnbull2015]. In validation, however, their four-gene, pre and on
treatment signature outperformed a similar pre-treatment only signature by
14\%[@Turnbull2015]. Continuing work by Sims *et al.* has combined NPI prognostic groups with
their two protein signature to further refine the predictive capabilities of
these on-treatment signatures [@SABCSposter]. 

## Thesis Hypothesis, Aims and Outcomes

This thesis aims to evaluate the worth of additional on-treatment sampling of
neoadjuvantly treated breast cancer patients for the purpose of biomarker
identification and risk stratification, seeking to prove the following thesis:

> *If on-treatment samples are more informative for the classification and
> characterization of breast cancer, then they should facilitate improved
> differential and statistical analysis of breast cancer.*

\noindent
Sequential sampling of these breast cancer cohorts should make available the
important and informative on-treatment expression level differences indicative
of response that can be captured and used for the future identification of
response and non-responsive patients to improve clinical options and further
the goal of personalized medicine. This project also aims to enumerate the
value multiple sampling in other settings, including matched primary and lymph
node biopsies to provide additional prognostic risk information. Lastly, in an
attempt to improve future analysis of breast cancer, this thesis aims to
exhaustively study the possibility of integrative analysis and/or comparative
meta-analysis to improve the resolution of treatment specific changes and more
accurately model and predict risk to patients. 
