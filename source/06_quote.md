
\newpage

\vspace*{\fill}

```

We who cut mere stones
must always be envisioning cathedrals.
    - Stone cutters's creed

```

\vspace*{\fill}

\newpage 
