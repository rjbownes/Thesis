# Perspectives, Discussion, and Conclusion 

An examination of The National Center for Biotechnology Information reveals
that there are over 505,000 papers returned for the query *Breast Cancer*, as
of October 2019. The prospect of adding new and novel work to this body of literature
poses an enormous task and the contribution of this thesis to that corpus must
be considered fully. While there are clear and distinct areas of prominent
research, such as biomarker discovery, new targets for therapy, and identification
of new actionable subtypes, my own analysis of the 100,000 most cited papers
reveals many more distinct groupings in this domain, see Figure
\ref{fig:i} on Page \pageref{fig:i}. Finding my own niche in this extensive
body of research helped to define the parameters of my work and lead me to the
results and output I have produced during my PhD. In this thesis, I seek to fully
explain the context of my research, its impact, and what this work may lead to in the
future.

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Downloads/example_clust.png}
    \caption{\textbf{UMAP reduced representation of breast cancer research.}
    100,000 breast cancer research papers were grouped by filtered content,
    authorship and citations to reveal distinct areas of research.}
    \label{fig:i}
\end{figure}

## Discussion

This thesis work is broken down into four discrete results chapters each
composed of modules of my work over the past four years but which in aggregate
represent a larger and more valuable message. Here I will discuss the interplay
between the chapters and begin to introduce the conclusions which should be
drawn from this body of work. I will highlight the poignant results of each
area of my thesis, the setting and surrounding literature, and potential avenues of
further research. A fundamental problem in the treatment of breast cancer is the heterogeneity of
the disease[@turashvili2017tumor]. Breast cancer doesn't represent one disease but many, each with
unique pathological and treatment options [@ellsworth2017molecular]. Indeed, even among a constituency of
similar tumours, patient variation can still occlude matched tumour
characteristics. In this regard, pairwise matched samples taken during
neoadjuvant therapy help to reduce the variance of the data set, and allow for
a remarkably granular view of patient risk on a personal level. This is due to
the fact that the inter-patient differences may be removed when considering
only the pairwise gene expression values from sequential samples within each
patient [@sabine2010gene]. 

&nbsp;

In addition to capturing the response of each
patient from the mid-chemo time point of a sequentially sampled cohort, it was also able to forecast the
survival of patients in both studies in a highly significant manner. This
clearly demonstrates that a marker generated on the delta values of gene
expression is able to capture response class specific changes to chemotherapy
that are uniform among patients and cohorts atleast partially due to the fact
that the signal to noise ratio present in the training samples is boosted by
removing the inter-patient variations. It should be noted that biomarker
identification is a topic of much scrutiny. Single novel biomarkers may be
subject to noise and variation and highly dependant on the subset of patients
used to identify the signature [@ein2004outcome]. There is also evidence that
random biomarkers may be predictive as many are associated with proliferation,
a positive indicator of response globally [@venet2011most] and that realistic
datasets for true identification of biomarkers would require massive sample
sizes [@ein2006thousands], the goal of chapter 4. An important take away is
more the value of the on-treatment samples and the extra information held in
the pairwise gene expression change values. In addition, it is strongly suggested
that the on-treatment samples may be of increased prognostic value from a
subtyping and stratification perspective, as the on-treatment samples had a
demonstrably differential expression profile and a greater ability to
"correctly" reflect the actual risk posed to each patient based on calculated
metrics of prognostic risk (MammaPrint, rorS). This is highly novel work, as
there is a paucity of preceding work in biomarker generation from on-treatment
samples, the majority of the accessible results have been included in this
manuscript already, primarily from Sims *et al.*. Markers of proliferation have
been previously studied for their ability to stratify patient risk, but this
work is tangential, not identical to the methods presented here. This does
raise the question of what additional biomarkers can be identified from these
matched patient samples, especially when larger uniform datasets become
available.

&nbsp;

Matched patient sequential samples of primary breast cancer are not the only
additional samples taken in the routine clinical and pathological appraisal of
BC. Tumour positive nodes are also frequently examined as a categorical risk
indicator for future recurrences and metastasis. However, as I explore in
chapter three, the same pairwise relative expression differences can be
utilized in this new setting to improve the capture rate of patient recurrence.
The gene expression level changes that facilitate the escape of the primary
tumour from it's tumour bed and to infiltrate beyond are over represented in
the tumour profile of the sampled lymph node. This differential pattern of
expression provides a more accurate sampling of the patient specific risk posed
by their metastatic disease and can potentially be useful in improving the
treatment of these especially high risk patients. In this regard the additional
sampling not for the purpose of observing the on-treatment gene expression
changes, but to identify the tumour's evolution beyond the bounds of it's
original tumour bed is what provides the new and requisite information for this
analysis. Pairwise concordance of primary BC and metastatic disease has
previously been examined for mutational differences [@schrijver2018mutation;
@moreno2019concordance] to understand the progression of disease and for
identifying new actionable targets [@varevslija2018transcriptome] and modern
studies have identified gene expression profiles between the two
[@iwamoto2019distinct]. However this is the first time that such a comparison
has been used to better estimate the risk posed by the primary diagnosis. The
data presented in this cohort is also the largest matched primary and lymph
node dataset currently available with follow up and on-treatment paired
biopsies. The availability of contemporary datasets like the Lawler trial have
greatly boosted the validity of the claims made from this analysis as the same
patterns of expression and relative risk are clearly present in this parallel
cohort.

&nbsp;

From a high level the premise of more samples, better results seems intuitive.
More powerful statistical analysis is possible, which means results can be
better supported and the impact of findings fundamentally of increased
integrity [@nayak2010understanding]. However, it isn't the static increase in dimensionality that
provides the importance of these samples, partially as the patient number is
not inflated, but the relative changes that grant new insight and understanding
to breast cancer treatment and patient options. In the first instance this new
information is gained by observing temporal changes in differential expression
while minimizing the noise present in the comparison of the data. This has been
previously examined to some extent where time scale analysis of gene expression
reveals new pathway enrichment information from these fold changes
[@bendjilali2017time]. These pairwise on-treatment difference have also been
suggestive of helping to identify the mechanistic drivers for aberrant
molecular pathways for patient and tumour characterization
[@androulakis2007analysis]. In the latter, new value is added by probing
biopsies to see a facet of the disease
that is not present in the primary tumour body, but more pronounced in the metastatic
reaches of the disease. Patient matched and sequential samples are rare
however, and no unified resource or repository is available to freely probe
on-treatment samples for further biomarker
identification or validation testing. The creation of MetaMatchedBreast to
improve the analytical prospects was inspired by this precise lack of
resources. 

&nbsp;

In my fourth chapter I prepare two references to compare the performance of
quantile normalization with ComBat batch correction with additive covariates of
integration. First, pre-treatment samples only, second, using no integration
methods and just retaining similar features. While integration of the
pre-treatment alone sample is supported by literary evidence as a viable means
of integration [@sims2008removal; @Turnbull_2015; @taroni2017cross;
@Larsen_2014], matched pre and on-treatment samples have never been integrated
as far as I am able to determine, hence the need to establish base lines for
performance. Integration of the underlying expression values was exhaustively
shown to be inadvisable from my results, but this lead to the creation of a new
bioinformatics resource for the examination of these matched patient samples.
This is a truly novel new tool for analysis as to date there are no available
such tools in the neoadjuvant or sequentially sampled space. Other existing
tools for meta analysis have focused exclusively on the aggregation and
analysis of the most available breast cancer data; pre-treatment only samples.
MetaMatchedBreast fills a niche not currently serviced by the existing tools
for breast cancer meta-analysis [@forero2019available; @toro2018imageo: @tcgaR;
@gendoo2019metagxdata]. This leads to the distinct prospect of future
improvements in the understanding of the mechanistic and pathway related
changed in response to therapy,

&nbsp;

MetaMatchedBreast refocuses instead on the most important features of this
data; the pairwise expression level changes. Here we avoid bringing together the
multiple distributions, and sub distributions of data present in each dataset.
Instead, by normalizing the values consistently and retaining the delta values between
each time point, the same-scale pre and on-treatment changes can be compared
across multiple studies. This facilitates the increased statistical
significance that this type of analysis has previously lacked, while
emphasizing the very features of this data that make them unique and
additionally valuable. This is possibly the most appropriate way of combining
this type of data as it has been suggested that integrative analysis falsely
conveys confidence on results from aggregated data [@nygaard2016methods].
Analysis of this data as pairwise pre and on-treatment values immediately
yielded results. Clustering analysis of patients with known response
highlighted that responsive chemotherapy and aromatase inhibitor therapy have
significantly different patterns of expression change. However, there was
dramatic overlap of the non-responsive patients from both groups. Upon
performing pathway analysis and enrichment scoring between the groups of
responsive and non-responsive patients, genes present in the mTOR and PI3K pathways were
significantly over expressed in the non-responders. While not definitive, this
is at least suggestive of common escape pathways in BC that may be utilized for
future drug discovery. These pathways highlight the resolution of known and
important pathways in breast cancer, mTOR [@tian2019mtor; @hynes2006mtor;
@steelman2016therapeutic] and PI3K [@serra2011pi3k; @qin2018impact;
@yang2019targeting] that were over represented in the non-responsive samples
and may represent an exploitable future avenue for improving the outcome of
these patients. This work is similar in some regards to other network analysis
of BC that has previously identified similar such pathways in non-responsive or
resistant patients[@berns2007functional]. However, the unique nature of this
pairwise difference patient data may help to elucidate further insights into
the causes of non-response. 

## Conclusion 

This thesis fundamentally set out to prove or disaprove the following
hypothesis:

> *If on-treatment samples are more informative for the classification and
> characterization of breast cancer, then they should facilitate improved
> differential and statistical analysis of breast cancer.*

In this regard this thesis has been successful in providing evidence for the additional
information available through the pairwise analysis of breast cancer from the
perspective of these samples. From my work in this thesis, I have shown clearly
that the on-treatment samples can be utilized for the creation of novel and
highly accurate biomarkers for the prediction of response to chemotherapy and
as a strong positive indicator of survival. In addition, these samples, and
other pairwise tissue sampling of metastatic lymph node tissue are
more informative for the use of existing risk scoring and profiling tests for
the prognostic stratification of patients. Lastly, that in aggregate, the
pairwise differences between the pre- and on-treatment samples can be a
powerful tool for pathway analysis to reveal new insights into the common
mechanisms of tumour response escape and identifying important features that
define these non-responsive patients.  
