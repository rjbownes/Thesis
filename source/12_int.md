# Evaluation of Approaches to Integrate Sequential Pre- and On-treatment Patient-Matched Breast Cancer Datasets 

\chaptermark{Comparison of Integration Methods}

## Abstract

\noindent
**Background**

&nbsp;

Neoadjuvant therapy represents a unique opportunity to monitor
tumour expression level changes and response to treatment in close to real
time. Changes in the transcriptomic landscape of cancer on-treatment have
already been shown to correlate with response and outcome in labeled patient
data from modestly sized studies. By combining several contemporary datasets,
can more significant findings can be reached to further the goal of personalized
medicine?

&nbsp;

\noindent
**Methods**

&nbsp;

Five transcriptomic datasets of primary breast cancer were collected
and combined using *ComBat* to create a unified expression dataset with matched
annotation data. Results of each different integration methods using *ComBat*
with different computational covariates were compared with unintegrated and
separately analyzed data to establish discordance, and to parallel methods to
evaluate performance.

&nbsp;

\noindent
**Results**

&nbsp;

Integration of patient-matched sequential sample datasets proved remarkably complex and
attempts with existing techniques exposed new challenges to large scale
integration. Biological and systematic covariates were evaluated to improve
batch correction methodologies, however, integration
of these datasets failed to return concordant values for expression, intrinsic
subtype or calculated risk scores (agreement scores for calculated subtype
ranged from 19-30%). Additionally, matched patient correlations were low
(median 0.62), and differential expression between the altered and unaltered
data was non-overlapping (mean 30.2% shared pathways), suggesting systematic
changes to the expression profiles post integration.

&nbsp;

\noindent
**Conclusion**

&nbsp;

This study evaluated methodologies for the integration of multiple sequentially
sampled data sets to create the largest uniformly annotated transcriptional
dataset of sequentially sampled neo-adjuvant primary breast cancer. However,
unifying transcriptional data across multiple studies proved an insurmountable
task to accomplish and maintain the underlying biological variation.
Alternatively, considering only the pairwise differences within each dataset
allowed for cross-study comparisons and improved analysis. This resulted in an
expression list object of similarly normalized data, with unified annotation
data for analysis and is presented to provide opportunities for on-treatment
biomarker identification and validation. This solution provides improved
statistical power for the identification of pan-treatment trends in breast
cancer and a pragmatic and workable solution for comparing and combining
datasets.

\newpage

## Background

\FloatBarrier

There is an abundance of pre-treatment only breast cancer gene expression data
sets like with 50-300 patients like the NKI[@van_t_Veer_2002], with a few
larger exceptions like METABRIC[@Curtis_2012] (2,506) the breast cancer specific section of the TCGA[@2012]) (987). Recently, there
have been made available tools and packages for
accessing this type of data from curated repositories (MetaGXData, specifically
Breast, [@gendoo2019metagxdata], and curateTCGA [@tcgaR]). There are
additionally tools leveraging the vast amounts of well annotated data on NCBI
GEO to allow for the bespoke creation and testing of meta-data sets like ImaGEO
[@toro2018imageo] which facilitate the acquisition and probing of specific,
relevant GEO accessions for analysis. While it is broadly understood that more samples and a more
representative cohort improves the
significance of results, these attempts to improve analysis
are all examples of meta-analysis and do not represent a true integration of
the data. These increase sample size and representation of the data, but
fundamentally only test a hypothesis or assertion in several datasets
independently, pool the results, and report the aggregate. Integration of -omic
data would assist in a new and improved understanding of the underlying
biological process and mechanisms of disease like cancer [@forero2019available]
but is a non-trivial undertaking. This has also never truly been undertaken for
sequential or neoadjuvant studies, even though large datasets of well annotated pre-treatment only data have resulted in
several novel approaches for breast cancer subtyping and risk assesment (70
gene signature, IC10, respectively), including FDA approval and expansive
clinical trials [@Cardoso_2016]. As of yet there are no large scale
datasets for matched patient samples or window studies.  

&nbsp;

Neoadjuvant therapy for primary breast cancer is showing significant results
for patient care and outcome, including higher rates of breast conserving
surgery without an increase in long term distant recurrence [@Derks_2018]. We
have already shown that there are distinct transcriptional differences on
treatment that can be used to differentiate responsive and non-responsive
primary breast cancer in neoadjuvant chemotherapy [@Bownes_2019] in two
modestly sized cohorts. Additionally, this work strongly suggests these samples
being of increased value to existing prognostic tests, however this required a
larger sample size to validate.  Additionally Turnbull *et al.* previously
showed that, in a similar fashion, samples from neo-adjuvantly treated
endocrine therapy patients could be used to create novel biomarkers for the
prediction of response to AI's [@Turnbull_2015]. These findings suggest that
the fold changes seen on-treatment in the neo-adjuvant setting for sequentially
sampled breast cancer may be of inherent value for the treatment and management
of breast cancer. Turnbull *et at.*  also perform the first, and singular
example to date, successful integration of multiple on-treatment datasets
[@turnbull2012direct]. That 2012 study examined the integration of two
different platforms, with very similar platforms, with the significant
advantage of replicate patient samples across the platforms to validate that
integration did not distort the expression values [@turnbull2012direct].


&nbsp;

Neoadjuvant trials are however comparatively scarce and there are additional
factors to consider for these samples, including the increased cost for multiple
biopsies and the associated increased patient stress. It is possible to justify
these risks with potentially significant improved insights into risk stratification and prognosis,
but integration is required for a sufficiently powered cohort.
This study proposes to trial integration techniques for patient matched
sequential samples of primary breast cancer to ascertain the viability of cross
study combination that will retain both the composition and biological
variation of the pre and post treatment samples but that will also leave the
per patient transcriptional pairwise matched pre- to on-treatment gene
expression level changes, delta values) intact. Figure \ref{fig:IntG} shows
the concept of an "independent" analysis of multiple datasets and the
integrative approach.


\begin{figure}[htp]
\centering
    \includegraphics[width=1\textwidth]{/home/richard/Downloads/COI.pdf}
    \caption{\textbf{Schematic Diagram Illustrating Independent vs. Integrative analysis.}
    Independent, or meta-analysis seeks to test one dataset and make inferences
    with data from another, or in this case, by testing multiple datasets
    simultaneously, and comparing the results. Integrative analysis attempts to
    first unify the data, then probe the data. This can some times lead to
    differences in the presentation of the data.}
    \label{fig:IntG}
\end{figure}

\FloatBarrier

### Integration Methods for Transcriptomic Data

Methods for the integration of continuous microarray expression data include
Surrogate Variable Analysis[@Leek_2007], Bayesian Factor Regression Models
[@Carvalho_2008], Factor Analysis [@Wang_2011] and Array Generation based gene
Centering[@Kilpinen_2008]. These methods all seek to shift the distributions
of expression towards one another and thus achieve integration. *ComBat*
[@Johnson_2006] has been established as the most widely used method for batch
correction and integration of microarray data [@M_ller_2016; @Chen_2011;
@Kupfer_2012] and will be utilised in this study as the primary tool for
integration. Recently, Cross Platform Normalization (XPN) has been shown to be
a reliable method of microarray integration for the purposes of machine
learning on microarray and RNAseq data [@taroni2017cross], but this is a new
result, and XPN designed for only two "batches" at a time. *ComBat* uses an
empirical Bayesian method with a combination of additive and multiplicative
terms to calculate the batch effect and remove
them from the gene expression data. Due to the over-representation of *ComBat*
in the literature of microarray and transcriptomic data integration, it is the
logical starting place for attempting to correct the effects of batch
introduced by the multiple different technical biological confounding factors
in this data. This technology does however rely on the assumption that the
expression data is in some way affected by these factors
[@Johnson_2006], this constraint will be examined for it's affect on the
resultant integration. We will discuss the steps required to integrate the
disparate datasets and the decisions made to analyse the affects of the
rationalized most important covariates of; dataset, biopsy time, Treatment type, and calculated
intrinsic subtype.

## Materials and Methods

\FloatBarrier

This study is comprised of five data sets of either neoadjuvantly chemotherapy
treated patients with matched pre-treatment, on-treatment, and surgical biopsy
samples, or aromatase inhibitor treated patients with similarly matching
samples. These are all available publicly available from NCBI GEO (GSE59515,
GSE20181, GSE87411, GSE122630, GSE32603), details of the total study size,
treatment architecture, biopsy schedule, and platform are contained in Table
\ref{tab:int_t_1} page \pageref{tab:int_t_1}. There is an imbalance of the
study design between the five cohorts, which is
reflective of the number of matched paired samples and the number of possible
matches, Table \ref{tab:int_t_1}. Comparisons of time points is only made cross-study where
direct comparisons are available. Additionally, the numbers of matched pairs
for each study are tabulated in Table Table \ref{tab:int_t_1}. Additional annotation information for
the data can be found in the relevant gitlab repository;
`rjbownes/integration_rationale/data/Annotation/annotationST.csv`,
which includes the full annotation for the combined cohorts, representing 33
features in its
entirety, including the initial calculated intrinsic subtypes, the full
available clinical and pathological features as well as study specific
parameters like biopsy schedule and study Id's. These studies were chosen
because they are among the largest and most well annotated patient matched
sequential biopsy studies publicly available. 

\begin{landscape}

\begin{table}[htp]

\centering

\resizebox{\textwidth}{!}{%

\begin{tabular}{|l|l|l|l|l|l|llll}

\hline

Data-Set & TP & T-On & TM & TS & Total & \multicolumn{1}{l|}{GEO}       & \multicolumn{1}{l|}{Study Structure}                                                                                       & \multicolumn{1}{l|}{Treatment}                                                                                              & \multicolumn{1}{l|}{Platform}                                                                                           \\ \hline

E1       & 131           & 110                & X                 & 132              & 373   & \multicolumn{1}{l|}{GSE122630} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Pre-treatment, \\ two weeks, \\ mid chemo, \\ surgical biopsy\end{tabular}} & \multicolumn{1}{l|}{FEC, Docetaxel}                                                                                         & \multicolumn{1}{l|}{Ion Ampliseq}                                                                                       \\ \hline

E2       & 58            & 58                 & X                 & 60               & 176   & \multicolumn{1}{l|}{GSE32603}  & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Pre-treatment, \\ 24-72 hours, \\ surgical biopsy\end{tabular}}             & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Anthracycline \\ followed by taxane\end{tabular}}                            & \multicolumn{1}{l|}{HAQQLAB\_Human\_40986}                                                                              \\ \hline

E3       & 109           & 97                 & 12                & X                & 218   & \multicolumn{1}{l|}{GSE59515}  & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Pre-treatment, \\ two weeks, \\ three months, \\ longer term\end{tabular}}  & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Anastrozole \&amp; \\ Letrozole\end{tabular}}                                & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Illumina HumanHT-12 V4.0, \\ Affymetrix Human Genome U133A\end{tabular}} \\ \hline

C1       & 34            & 12                 & 24                & 25               & 95    & \multicolumn{1}{l|}{GSE20181}  & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Pre-treatment, \\ 10-14 days, \\ surgical biopsy\end{tabular}}              & \multicolumn{1}{l|}{Letrozole}                                                                                              & \multicolumn{1}{l|}{Affymetrix Human Genome U133A}                                                                      \\ \hline

C2       & 141           & 45                 & X                 & 62               & 248   & \multicolumn{1}{l|}{GSE87411}  & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Pre-treatment, \\ 2-4 weeks\end{tabular}}                                   & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Anastrozole, \\ exemestane, \\ letrozole, \\ FEC, \\ docetaxel\end{tabular}} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Agilent-014850 Whole Human Genome \\ Microarray 4x44K\end{tabular}}      \\ \hline

Total    & 473           & 322                & 36                & 279              & 1110  &                                &                                                                                                                            &                                                                                                                             &                                                                                                                         \\ \cline{1-6}

\end{tabular}%

}

\caption{\textbf{Available samples and study features for each dataset.} Not all
samples were available at every time point, this shows the available samples at
each biopsy time, an X signifies no biopsies at that time point.}

\label{tab:int_t_1}

\end{table}

\end{landscape}

\FloatBarrier

### Data Selection and Acquisition 

Collection of the data was primarily acquired with GEOquery [@geo] from inside
R collecting the expression data, annotation, and GPL data with the exception
of E1 which is comprised of two partially overlapping studies of Illumina and
Affy data, the contents, description of that work is detailed by *Turnbull et.
al* [@Kitchen_2011]. Preprocessing and normalization of the data was handled in
R [@R], with the standard library and Bioconductor [@BIO] packages Limma
[@Limma] and edgeR [@edge1; @edge2], subtyping and risk assessment were
performed by the Genefu package [@genfu].  Unsupervised cluster analysis was
performed with base R `prcomp` and all visualization was handled by ggplot2
[@ggplot]. Data manipulation and cleaning was performed primarily within
tidyverse [@tidy], data packaging and function wrapping was done using using
base R [@R] and Biobase [@BIO], as well as XDE [@XDE] for custom S4 data types,
and oxygen2 [@oxy] for documentation.

### Preprocessing, Normalization, and Analysis of Expression Data

Each dataset was initially cleaned of low coverage and missing probes and
preprocessed individually, read counts were converted to counts per million and
filtered for genes with expression in at least half the samples to avoid
inclusion of genes with no recorded expression level data (NAs), this was
performed entirely in R by Limma [@Limma] and edgeR [@edge1; @edge2]. Library counts
were summed for each sample to ensure that each passed a per-dataset threshold
and was not significantly larger or smaller than it's contemporaries. Datasets
were then all voom normalized to normalize the datasets on the same features,
Law *et al.* describe this process in great detail and establish voom as a
viable method for heterogeneous data processing [@law2014voom]. Initial
subtypes and risk scores for IC10 [@IC101; @IC102; @IC103], Pam50 [@IC103],
scmod1 [@desmedt2008biological] and scmod2 [@wirapati2008meta], ssp2003
[@IC101] and ssp2006 [@IC102] , MammaPrint [@van_t_Veer_2002] and rorS [@IC103]
continual and categorical risk values were calculated with Genefu. Numerical comparisons
were made using the `compareDF` [@compare] package in R and tabulated for
comparison.

### The Origins of Technical and Biological Variance

\FloatBarrier

There are numerous confounding factors to consider for cross platform
comparisons, each of which must be accounted for and minimized during the
integration process, see Figure \ref{fig:IntF}. Each of these factors may be of greater magnitude than
the important biological processes and differences that we seek to emphasize by
aggregating the data but that may be further diminished by the addition of
noise from this exact process. The impact of these factors will be analyzed
post integration in three key ways; with single time point pre-treatment only
sample integration, non-informed uncorrected joining methods, and *ComBat* correction
with additional covariate information for each of these deleterious artefacts.

\begin{figure}[htp]
\centering
    \includegraphics[width=1\textwidth]{/home/richard/Downloads/IntegrationFigure(1).pdf}
    \caption{\textbf{Concept Map of the various biological and technical
    factors to consider for integration.} This diagram highlights some of the
    considered factors for convariates of ComBat analysis
    due to their intrinsic ability to convolute cross study comparisons. This
    diagram attempts to capture the nuance of this analysis and is generalising
    concepts, this does represent individual samples from each dataset, or
    encapsulate the relative dataset size or the proportion of responders or
    subtypes.}
    \label{fig:IntF}
\end{figure}


\FloatBarrier

### Pre-Treatment only Integration as Reference Performance Data

Integration of microarray data of pre-treatment only data in the adjuvant
therapy setting is an established practice with early investigations
identifying and accounting for the multiplicative distortions introduced in the
routine processing of microarray data [@sims2008removal]. Subsequent work by
Turnbull *et al.* showed, for example, the cross platform normalization of Affy
and Illumina data using XPN and *ComBat* [@Turnbull_2015], or Greene *et al.*
for a further instance of XPN's application to the simultaneous evaluation of
microarray and RNAseq data [@taroni2017cross]. Removing batch effects with
techniques like *ComBat* have been shown to facilitate cross platform
expression comparison [@Larsen_2014]. The pre-treatment samples were integrated
using only the dataset as the batch multiplicative factor of integration. Performance
of *ComBat* in this setting will be quantified to provide a reference for the
amount of distortion added by the inclusion of the on-treatment samples.


### Uncorrected Integration Methods  

\FloatBarrier

Uncorrected approaches to dataset integration were performed with subset 
(retaining only the expression features common to all data sets) and complete
(retaining all features across all datasets) joins of data. Missing genes post join were filled with added
interpolation of missing features to preserve dimensionality (i.e. keep full
gene annotation) across datasets, this was performed with K-Nearest-Neighbor
imputation, Figure \ref{tab:impute} highlights how these values are calculated,
where the K nearest neighbors are located and the missing values are filled in
with the mean value from the N neighbors.
Quantile normalization[@M_ller_2016] was used to retain the ranking of gene expression on a
per dataset basis, while converging the
expression distributions. Multiple tests were then used to score the performance of these methods
and used as a second reference of performance for the *ComBat* assisted pre-
and on-treatment sample integration.

  \begin{figure}[htp]
    \centering
    \includegraphics[width=1\textwidth]{/home/richard/Integration_Rationale/Rplot39.pdf}
    \begin{tabular}[b]{l|l|l|l|l|l|l|}

\cline{2-7}

                             & \multicolumn{3}{l|}{Samples with NA's} & \multicolumn{3}{l|}{Imputed Values} \\ \cline{2-7} 

                             & Sample 1    & Sample 2    & Sample 3   & Sample 1 & Sample 2 & Sample 3      \\ \hline

\multicolumn{1}{|l|}{Gene 1} & 8.52        & 11.23       & NA         & 8.52     & 11.23    & \textbf{9.88} \\ \hline

\multicolumn{1}{|l|}{Gene 2} & 9.12        & 7.47        & 8.44       & 9.12     & 7.47     & 8.44          \\ \hline

\multicolumn{1}{|l|}{Gene 3} & 7.98        & 9.20        & NA         & 7.98     & 9.20     & \textbf{8.59} \\ \hline

\end{tabular}
    
    \captionlistentry[table]{A table beside a figure}
    \caption{\textbf{Example Imputed Values from Continuous Gene Expression Data.} KNN imputation works by finding the K nearest neighbors, averaging the values from those neighbors and supplying them as the missing values.}
    \label{tab:impute}
  \end{figure}


&nbsp;

As previously stated, complete observational and subset joins will result in dramatically
different dimensions for the product dataframe. While the number of patients
will not differ between the two methods, the feature list (rows) of the
integrated data will reflect the difference in the mechanics of each join. In
the case of this translational data, the features are genes. The resulting size
of the gene lists are plotted below as venn diagrams. The final
dimensions for each join are: Full outer join, 1122 samples with 21075 gene
annotations, inner join 1122 samples with 4786 gene annotations.

\begin{figure}[htp]
\centering
    \includegraphics[width=1\textwidth]{/home/richard/Integration_Rationale/checkkkk.pdf}
    \caption{\textbf{Comparison of the numbers of commonly represented genes
    when, 2, 3 or 5 datasets are combined.} The 2 lobed venn-diagram is the overlap of the two chemotherapy cohorts, the 3 lobed venn-diagram represents the overlap of the 3 endocrine therapy cohorts, and the 5 way venn-diagram is the overlap of the features present in all five datasets.}
    \label{fig:Int7}
\end{figure}

\FloatBarrier

### Reproducible *ComBat* Workflows and Integration Testing

Integration of the pre- and on-treatment data was performed using the
pre-selected covariates of analysis; Platform (P), Therapy (TT),
Time (TS), and Subtype (ST). Purrr [@purrr] was used to functionalize
the testing process and perform the integration on all possible combinations of
integration covariates.

### Patient Matched Concordance and Correlations as Integration Metrics

Patient matched correlation, heatmaps with hierarchical clustering, principal
component analysis, intrinsic subtype concordance, differential gene
expression, machine learning classification and pre- to on-treatment
proliferation delta values were compared to establish
discordance between the reference data and each iterative integration attempt. Heatmaps were generated for every *ComBat* integration attempt using the R package
`pheatmap` [@pheatmap] with hierarchical sample clustering and per patient
annotation of the different biological and systematic features. Initial
subtypes and risk scores for IC10 [@IC101; @IC102; @IC103], Pam50 [@IC103],
scmod1 [@desmedt2008biological] and scmod2 [@wirapati2008meta], ssp2003
[@IC101] and ssp2006 [@IC102] , MammaPrint [@van_t_Veer_2002] and rorS [@IC103]
continual and categorical risk values were calculated. Numerical comparisons
were made using the `compareDF` [@compare] package in R and tabulated for
comparison. Machine learning methods for the classification of cancers [@he2007ranked] and
the prediction of recurrence of breast cancer [@al2018breast] is well
established. In particular, random forest has been shown to be a robust
algorithm for pattern detection [@al2018breast].

## Results | Unintegrated Data Meta-analysis 

In order to understand how each attempt at integration affected the underlying
biology of the patient samples baseline measurements were required as a
reference. The following sections highlight the important diagnostic results
from these baseline tests to help make future comparisons.

### Subtype Composition of Unintegrated Datasets

\FloatBarrier

The composition of the unintegrated data is presented in Figure \ref{fig:Int1},
page \pageref{fig:Int1} as pairwise risk score
comparisons, annotated by the color of the initial intrinsic subtype
calculation. Four important pieces of information are contained in this
diagram. First, there are relatively more treatment matched patient samples for the AI treated datasets. Second, there
is a trend in the data for a reduction in the calculated risk of the samples on
treatment compared to pre, this is evidenced by the position of the sample lower on
the Y axis (On-Treatment) relative to their corresponding X axis
(Pre-Treatment). Third, there is an over representation of Basal
subtypes in the chemotherapy cohorts compared to the AI treated cohorts and
lastly, the most significant reductions in risk are those of the on-treatment
Luminal B samples in the AI datasets (wilcoxon p < 0.05 for mean reduction). Comparisons of all
8 classifiers (pam50, ic10, ssp2003|6, smcgene, mammaprint, rorS, scmod1/2) will be made, with results presented as concordance between the
pre and post integration values on a per patient basis.

\begin{figure}[htp]
\centering
    \includegraphics[width=1\textwidth,scale=2]{/home/richard/Integration_Rationale/Rplot74.pdf}
    \caption{\textbf{Independent Analysis of the Subtype Composition Across Multiple Datasets.} This figure illustrates the
    pre integration patterns of predicted risk change with treatment. The X and Y coordinates are the TP rorS
    risk and T-On rorS risk, respectively. Each sample is colored by their
    Pam50 assignment at TP to illustrate subtype specification changes.}
    \label{fig:Int1}
\end{figure}

\FloatBarrier

### Continuous and Categorical Risk Classification Across Multiple Independent Datasets 

\FloatBarrier

The relative risk and relapse scores for each of the five datasets were
examined using MammaPrint and PAM50 rorS. These scores were calculated to
identify differences in the presentation of the datasets and to act as a
reference for the changes in the apparent risk associated with each dataset
after each method of integration. The median risk score and the tally
of categorical risks classifications for each dataset according to both rorS
and MammaPrint is presented in the following Table \ref{tab:int_t_2}, page
\pageref{tab:int_t_2}. Clinically it is
interesting to see that the chemotherapy prescribed cohorts are of higher
average risk across both tests, with a greater proportion of categorical high
risk samples. The two chemotherapy cohorts were comprised of patients with a
worse general prognosis and average higher histological grade. Deviation from these values, continuous or categorical will
inform us on the affects of integration to the prognostic appearance of the
patient samples.


\begin{table}[htp]

\centering

\resizebox{\textwidth}{!}{%

\begin{tabular}{|l|l|l|l|l|}

\hline

Dataset & MammaPrint &  M.Score & rorS &  r.Score \\ \hline

E1 & 0(175), 1(187) & -0.29 & L(141), I(111), H(110) & 36.3 \\ \hline

E2 & 0(0), 1(176) & -0.04 & L(35), I(49), H(92) & 54.6 \\ \hline

E3 & 0(191), 1(27) & -0.45 & L(116), I(46), H(56) & 26.1 \\ \hline

C1 & 0(29), 1(66) & -0.23 & L(35), I(24), H(36) & 40.8 \\ \hline

C2 & 0(56), 1(192) & -0.004 & L(62), I(55), H(113) & 55.8 \\ \hline

\end{tabular}%

}

\caption{\textbf{Continuous and Categorical Risk Assessment Across Multiple
Independent Datasets.} This table presents the estimated risk associated with each data set. The
chemotherapy datasets were  of generally higher average risk and had more
categorical high risk patients.}

\label{tab:int_t_2}

\end{table}

\FloatBarrier

### Pre-integration PCA Visualization Data

\FloatBarrier

PCA of the unintegrated data is presented Figure \ref{fig:Int2}, page
\pageref{fig:Int2}. The affect of a platform batch effect is clearly visible in
the PCA representation. As expected with highly
dimensional data the amount of explained variance is low, at 17.2% and 12.1% in PC1/2 respectively.

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/Rplot60.pdf}
    \caption{\textbf{PCA Diagram of the Unintegrated Data Illustrates the Strong Presence of Platform Associated Batch.} This PCA visualization represents
    the five datasets, colored by the cohort of origin. It is clear
    from this initial analysis that there is distinct seperation of the data on
    the basis of batch.}
    \label{fig:Int2}
\end{figure}

\FloatBarrier

### Principal Components Analysis of Independent Unintegrated Data

\FloatBarrier

Some of the genes that comprise PC1 and PC2 for each data set pre-integration
are presented in Table \ref{tab:int_t_3}, page \pageref{tab:int_t_3}. These
gene lists will be use for later comparison of Principal Component outputs to
enumerate the degree of change caused by each integration attempt.

\begin{table}[htp]

\centering

\resizebox{\textwidth}{!}{%

\begin{tabular}{|l|l|l|}

\hline

Dataset & Genes in PC 1 & Genes in PC 2 \\ \hline

E1 & HSPB6, MYH11, PLIN1, RBP4, EGR1 & AGR3, AGR2, MUCL1, SCGB2A2, TFF3 \\ \hline

E2 & TTK, DUS1L, DHX33, CDCA8, C10orf54 & TFF3, KRT19, KIAA1324, FOXA1, RARRES3 \\ \hline

E3 & LYPLA1, ARMT1, TMEM106B, CAPZA2, VPS4B & DPT, FABP4, FOS, ACKR1, C1S \\ \hline

C1 & RPL41, RP5, RPS23, EEF1A1, RPL13A & COX6C, CPB1, PIP, CARTPT, SLC39A6 \\ \hline

C2 & RGS1, MNDA, EHF, SPARCL1, MYSM1 & NKAIN1, SYT13, DSCR6, PDZK1, GRIK3 \\ \hline

\end{tabular}%

}

\caption{\textbf{Contents of PC1 and PC2 for future comparison.} Top five
contributors of the first and second principal components for each dataset are
listed above, these are primarily a resource for comparing the change in the
variance as a product of integration. The biological relevance of these gene
lists has not been examined as the method for identifying them is not to
elucidate biological processes or pathways but as a comparative metric.}

\label{tab:int_t_3}

\end{table}

\FloatBarrier

## Results | Correlation Analysis Reveals Changes in Patient Expression Profile As a Product of Integration

Pearson correlation of matched patient samples was calculated using the base R
`cor` function to establish high level similarity between samples pre and post
integration. This was used as a metric to gauge the similarity or dissimilarity
of the full transcriptomic profile of each patient sample.

### Pre-treatment Samples Corrected with Platform ComBat Batch Correction

\FloatBarrier

Correlation of individual samples is presented in Figure \ref{fig:Int5}, page
\pageref{fig:Int5} showing how patients with differing similarity appear for
reference post integration. Matched patient correlation on a per-dataset level
is presented in Figure \ref{fig:Int6}, page \pageref{fig:Int6} to visualize the
correlation of pre-treatment samples pre and post-integration. Correlation
values vary between the datasets, but are consistently high (75% > ). It is
difficult to establish a firm reference metric for acceptable correlation
values, but this level of correlation is strongly indicative of a trend.

\begin{figure}[htp]
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/Rplot44.pdf}
    \caption{\textbf{Example Correlation Diagrams of Matched Patient Samples}
    A) Shows a hypotehtical correlation of 1, indicating no change in the pre
    and post integration samples. B) A correlation of 0.88, indicating
    a low level of distortion between the pre- and post-integration sample. C)
    and D) are samples with a correlation of 0.22 and 0.24 respectively, which would be a very
    low correlation suggesting a large amount of integration distortion.}
    \label{fig:Int5}
\end{figure}

\begin{figure}[htp]
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/Rplot45.pdf}
    \caption{\textbf{Correlation of pretreatment only Combat platform corrected integration data} Aggregrated
    correlation values on a per dataset level show the changes that are caused
    by the integration on a superficial and unsupervised level. There is a
    high, but inconsistent correlation of the data pre to post integration when
    considering only the pre-treatment samples.}
    \label{fig:Int6}
\end{figure}

\FloatBarrier

### Uncorrected Combination of Sequentially Sampled Pre- and On-treatment Datasets 

\FloatBarrier

Figure \ref{fig:Int8} page \pageref{fig:Int8} shows the per dataset correlation
for the uncorrected feature joining methods. Both methods showed similar levels of
average correlation to the pre-treatment only (calculate mean correlation). While there is a marked difference in the
correlation of each cohort, E2 is the most tightly correlated for both methods,
and with the exception of E3, there is little statistical difference in the
average correlation of the data (all pvalues <0.05).

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/long_short_cor.pdf}
    \caption{\textbf{Matched Patient Sample Correlation for Uncorrected Feature Joining Methods.} Correlation of patients to their matched samples post
    combination by complete (left) and subset joins (right) show the resultant
    per-patient correlation represent on a per-dataset level. This shows a
    clear drop in correlation post combination, with the exception of E2.}
    \label{fig:Int8}
\end{figure}

\FloatBarrier

### *ComBat* Integration of Sequential Samples with Multiple Covariates 

\FloatBarrier

It is clear from Figure \ref{fig:10}, page \pageref{fig:10} that there is a
range of correlations. The E2 cohort is of significantly higher correlation
with it's own matched preintegration samples than the other cohorts (p < 0.05).

\begin{figure}[htp]
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/quad_cor_int.pdf}
    \caption{\textbf{Correlation pre- and post-integration are lower for all
    ComBat covariates.} Correlation is stable across
    the four integration methods (Platform (A), Time (B), Therapy (C) and
    subtype (D) and comparatively low when compared to either uncorrected
    methods, or the pre-treamten only sample integration. E2 is a constant outlier in correlation,
    there is compounding interest that there is a systematic
    reason for this but it is difficult to discern precisely.}
    \label{fig:10}
\end{figure}

\FloatBarrier

## Results | PCA Analysis Visualises Changes to the Relative Expression of the Gross Cohort Expression Profile 

PCA analysis reveals how the variance within each dataset is affected by
integration. This is primarily judged on the graphical
representation of the samples and the appearance of clustering and group
separation. Diagrams of the first and second principal components were created
to visualize the change in variance and evaluate the performance of the
integration methods. Additionally, gene lists that describe the first and
second principal components of the dataset where compared pre and post
integration to examine the similarity in explained variance.


### Pre-treatment Samples with Platform ComBat Batch Correction


\FloatBarrier

The 2D representation of the first two principal components can be seen in Figure \ref{fig:Int3}, \pageref{fig:Int3}. This was produced using only the
multiplicative variable of Platform, and making non-parametric estimation to
the prior distributions. This attempt at integration is visually conforming to the expected *ComBat* output.

\begin{figure}[!htp]
  \centering
  \begin{minipage}[b]{0.7\textwidth}
    \includegraphics[width=\textwidth]{/home/richard/Integration_Rationale/Rplot60.pdf}
    \caption{\textbf{Integration of Pre-treatment only samples using QN and
    ComBat} Integration of the pre-treatment only samples clearly shows normal
    and expected integration using the literature established methods of
    integration for transcriptomic data. For reference, the pre-integration
    uncorrected PCA results are placed below for comparison.}
    \label{fig:Int3}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{1\textwidth}
    \includegraphics[width=\textwidth]{/home/richard/Integration_Rationale/Rplot54.pdf}
  \end{minipage}
\end{figure}

Pretreatment only sample integration resulted in an average retention of PC1
gene lists of 53.6% and PC2 of 32.12%, Table \ref{tab:pca_tab}, page \pageref{tab:pca_tab}. This result indicates that in the two
eigenvectors explaining the variance of the data, there is at best a similarity
of about a half and a third of the same genes.

\begin{landscape}

\begin{table}[htp]

\centering

\resizebox{\textwidth}{!}{%

\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|}

\hline

 & \multicolumn{2}{l|}{Pre-Treatment Only} & \multicolumn{2}{l|}{Complete} & \multicolumn{2}{l|}{Subset} & \multicolumn{2}{l|}{Platform} & \multicolumn{2}{l|}{Time} & \multicolumn{2}{l|}{Therapy} & \multicolumn{2}{l|}{Subtype} \\ \hline

 & PC1 & PC2 & PC1 & PC2 & PC1 & PC2 & PC1 & PC2 & PC1 & PC2 & PC1 & PC2 & PC1 & PC2 \\ \hline

C1 & 52.2\% & 32.4\% & 11.8\% & 0\% & 0.2\% & 1\% & 0.2 & 0\% & 11.6\% & 4\% & 0.4\% & 10.4\% & 10.8\% & 0.4\% \\ \hline

C2 & 61.6\% & 19.8\% & 68\% & 5\% & 0\% & 23.6\% & 2\% & 0.4\% & 60.2\% & 14\% & 0\% & 20.8\% & 65.8\% & 18.6\% \\ \hline

E1 & 42.7\% & 14.1\% & 33.6\% & 7\% & 28.6\% & 8.8\% & 0.8 & 0\% & 22.6\% & 1\% & 23.4\% & 47.2\% & 23.4\% & 1.2\% \\ \hline

E2 & 82\% & 21\% & 62.8\% & 14\% & 18.4\% & 26.6\% & 1.6 & 0\% & 15.2\% & 11.8\% & 59.4\% & 5.2\% & 27.4\% & 14.2\% \\ \hline

E3 & 29.5\% & 73.3\% & 18.4\% & 26.6\% & 1\% & 0\% & 0.6 & 1\% & 20.2\% & 37.8\% & 14.8\% & 35\% & 16.4\% & 37.4\% \\ \hline

\end{tabular}%

}

\caption{\textbf{Principal Component overlap between reference data and each
integration method.} Each row contains the percentage of genes present in the
first and second principal component of the post integration data present in
the preintegration data for each combination method.}

\label{tab:pca_tab}

\end{table}

\end{landscape}


\FloatBarrier


### Uncorrected Combination of Sequentially Sampled Datasets 


\FloatBarrier

Principal component analysis of these two methods is shown in Figure
\ref{fig:9}, \pageref{fig:9}. It is clear from the separation of the groups that the batch
effect is still present and that normalization alone has not reduced the
intergroup differences at all. The scale of the variational distance is
different between the two gene lists in PC1, this is likely due to the feature
size of the data but despite this, the pattern of clustering is comparable. The percentage of overlap of
the first and second principal components is presented in
Table \ref{tab:pca_tab}, page \pageref{tab:pca_tab}. There is an average PC1
overlap of 38.92% and an average PC2 overlap of 10.52%, comparably lower than
the pretreatment only integration values.

\begin{figure}[htp]
  \centering
  \begin{minipage}[b]{0.6\textwidth}
    \includegraphics[width=\textwidth]{/home/richard/Integration_Rationale/Rplot60.pdf}
    \caption{\textbf{Uncorrected Combination Methods Do Not Improve Dataset Integration} Changing the
    contents of the gene lists for the resultant dataset changes the variance
    contributions of each principal component. Here the variance is visually
    represented in a PCA diagram, and the difference in inter-dataset distance
    are clearly apparent between the two methods.}
    \label{fig:9}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{.9\textwidth}
    \includegraphics[width=\textwidth]{/home/richard/Integration_Rationale/Rplot81.pdf}
  \end{minipage}
\end{figure}

\FloatBarrier


### *ComBat* Integration of Sequential Samples with Multiple Covariates 


\FloatBarrier

Principal component analysis of the post integration data reveals significantly
less visual overlapping of the datasets than the pre-integration only or uncorrected
methods, Figure \ref{fig:Int12}, page \pageref{fig:Int12}. Additionally, with
increased feature count for the additive covariate there is increased
fragmentation of the resultant PCA plot. With respect to the pre-treatment
only, there is a clear loss of result integration, and compared to the uncorrected
joins there is an increase in the number of visible batches. Table
\ref{tab:pca_tab}, page \pageref{tab:pca_tab} contains the overlap values of the
first and second principal component, there is a mean overlap of PC1 and PC2 of
1.04 and 0.28, 25.96 and 13.72, 19.6 and 23.72, 28.76 and 14.36 for the
covariates of platform, treatment time, treatment type and subtype
respectively.

\begin{figure}[htp]
  \centering
  \begin{minipage}[b]{.6\textwidth}
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/Rplot60.pdf}
    \caption{\textbf{PCA plot comparisons of four integration methods.}
    Comparing PCA plots different integration methods Platform, Time, Therapy, and Subtype reveal a decomposition of the
    original batches with each method. How there is no increased distribution
    overlap.}
    \label{fig:Int12}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.9\textwidth}
    \includegraphics[width=\textwidth]{/home/richard/Integration_Rationale/Rplot56.pdf}
  \end{minipage}
\end{figure}

&nbsp;

Theoretically, correlation between principal components should be low
as they are calculated from non-correlated, orthogonal eigenvectors with
unique contributions from each gene. As can be seen in Table \ref{tab:int_t_8},
page \pageref{tab:int_t_8}, this trend is true for all data sets except for E2.
The collinearity seen in E2 shows that the features that define PC1 are related to the features in PC2,
indicating that variance in one plain effects variance in another.

\begin{table}[htp]

\centering

\resizebox{\textwidth}{!}{%

\begin{tabular}{|l|l|l|l|l|}

\hline

Dataset & Platform & Time & Therapy & Subtype \\ \hline

C1 & 12.9\% & 13\% & 15.1\% & 15.0\% \\ \hline

C2 & 6.7\% & 9.2\% & 13.1\% & 11.6\% \\ \hline

E1 & 9.1\% & 8.7\% & 11.8\% & 11.2\% \\ \hline

E2 & 87.2\% & 89.6\% & 86.8\% & 89.7\% \\ \hline

E3 & 14.1\% & 12.2\% & 15.3\% & 11.1\% \\ \hline

\end{tabular}%

}

\caption{\textbf{Measurement of collinearity between PC1 and PC2} There was a
strange amount of collinearity between what should be orthogonal vectors in
dataset E2, which possibly accounts for its outlier status in this and other
analyses.}

\label{tab:int_t_8}

\end{table}

\FloatBarrier


## Results | Heatmaps are Complementary Tools for Understanding the Cluster Analysis and PCA Results

In concert with the PCA diagrams, they can highlight groupings based on the
clinical and pathological features available for analysis. Heatmaps of the
pretreatment only sample and uncorrected integration methods were omitted due to the
PCA diagram results. Heatmaps were generated for every *ComBat* integration attempt using the R package
`pheatmap` [@pheatmap] with hierarchical sample clustering and per patient
annotation of the different biological and systematic features. These were used
for visual inspection of the effectiveness of *ComBat* and to estimate the
level of integration based on the presence or absence of noticeable groupings
of the annotated features.



### *ComBat* Integration of Sequential Samples with Multiple Covariates 


\FloatBarrier

Post integration heatmaps of the combined datasets were plotted to visualise
the representation of the biological and systematic annotation features among
the resultant batches. As Figure \ref{fig:11}, page \pageref{fig:11} shows, there
is no significant clustering of any
of the annotation features relevant to this analysis. There is some mixing of C2 (Blue) and E1 (green) upon integration with with
covariates of Platform, treatment type and subtype, however the datasets are
not integrated with respects to the treatment received, nor the patients by
response or treatment time. Fundamentally there appears to be
present batch effects in the post integration that are not corrected by the
integration methods.

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Thesis/source/figures/Int1_figs/grouped_figs/heat_2.pdf}
    \caption{\textbf{Heatmaps and hierarchical clustering of post integration
    datasets.} Heatmapping and clustering can help to identify patterns, or
    visually confirm results of analysis. Here, the patterns of non-integration
    are clear from the distinct expression patterns per dataset, and the
    visible lack of integration of the annotaion features. HM A shows the
    clustering an expression patterns for Platform integration, B for Time, C
    for Therapy and D for Subtyping integration.}
    \label{fig:11}
\end{figure}

\FloatBarrier

## Results | Assessment of molecular subtype assignment and prognostic score concordance 


### Pre-treatment Samples with Platform ComBat Batch Correction


\FloatBarrier

Subtyping and concordance of the resultant molecular/intrinsic subtypes is
shown below in Figure \ref{fig:Int4}, page \pageref{fig:Int4} for Pam50, rorS
risk classification, MammaPrint risk classification, iC10 subtyping and
ssp2003/6. Agreement of the calculated subtype is relatively high (concordance
> 80%, Table \ref{tab:conc_tab}, page \pageref{tab:conc_tab}) for all tests except iC10 which was in agreement 72% of the time. This
result shows that the concordance post integration is sensitive to the number
of classifications possible for each test, where the greater the number of
outcomes, the lower the concordance. These values show results in-line with
patient matched samples from different tissues or different treatment time
points (Chapters 2 and 3), indicating that the distortion of *ComBat* is at
least no greater than the effect of treatment. 

\begin{landscape}

\begin{table}[htp]

\centering

\resizebox{\textwidth}{!}{%

\begin{tabular}{|l|l|l|l|l|l|l|l|l|}

\hline

   & Pretreatment only samples  & Pretreatment Integration & Samples                    & Complete join & Platform & Time & Therapy & Subtype \\ \hline

C1 & 34                         & 0.15                     & 95                         & 0.71                  & 0.77     & 0.81           & 0.79           & 0.7     \\ \hline

C2 & 141                        & 0.23                     & 248                        & 0.79                  & 0.85     & 0.84           & 0.79           & 0.83    \\ \hline

E1 & 128                        & 0.19                     & 385                        & 0.8                   & 0.82     & 0.86           & 0.9            & 0.87    \\ \hline

E2 & 58                         & 0.22                     & 176                        & 0.54                  & 0.625    & 0.76           & 0.57           & 0.73    \\ \hline

E3 & 109                        & 0.1                      & 218                        & 0.82                  & 0.95     & 0.97           & 0.98           & 0.97    \\ \hline

   & \textbf{Mean}              & 0.18                     & \textbf{Mean}              & 0.74                  & 0.8      & 0.85           & 0.8            & 0.82    \\ \hline

   & \textbf{Average retention} & 82                       & \textbf{Average retention} & 26                    & 19.6     & 15             & 19.2           & 18      \\ \hline

\end{tabular}%

}

\caption{\textbf{Average loss of subtyping agreement per dataset for every
method.} Each row contains the average loss of concordance for every data set
for every test. The rows for mean represent each tests average performance,
Average retention inverts the result to show the number of samples on average
that experience no change.}

\label{tab:conc_tab}

\end{table}

\end{landscape}

\FloatBarrier


### Uncorrected Combination of Sequentially Sampled Datasets 


\FloatBarrier

For the data of the subset feature joins, no subtyping was available as the
reduced gene lists were too incomplete to conduct these profiling tests. For the full outer joined data, comparisons were
drawn for every sample from every dataset with the Genefu results from the
unintegrated data, Figure \ref{fig:naive_subs}, page \pageref{fig:naive_subs}. Full comparison tables are available at
`https://gitlab.com/rjbownes/Integration_Rationale/Compare_df`. A summary of
the differences is presented below in \ref{tab:conc_tab}, page \pageref{tab:conc_tab}. As no assumptions were made as to the relative importance of the
subtyping/classification tests, a non-concordant subtype in any of the Genefu
tests resulted in a step change reduction in agreement. Agreement of the pre
and post integration subtyping was low at a mean value of 26.8% unchanged
samples. Additional rorS and MammaPrint continuous risk score comparisons were
made, which clearly show a systematic decrease in the presented risk post
integration.

\begin{sidewaysfigure}[htp]
    \includegraphics[width=0.7\textwidth]{/home/richard/Downloads/NN1.pdf}
    \caption{\textbf{subtype concordance of pre-treatment integrated data}
    Presented here are tiled plots representing the agreement between the
    pre-integration and post integration genefu subtyping analysis results for
    six different tests; in descending order Pam50, ssp2003, ssp2006, iC10,
    scmod1, scmod2 and each column represents one dataset; C1, C2, E1, E2, E3.
    What is clear from an initial observation is that concordance between
    integration states is relatively high and is consistent between the data
    sets.}
    \label{fig:Int4}
\end{sidewaysfigure}


\begin{sidewaysfigure}[htp]
    \includegraphics[width=0.7\textwidth]{/home/richard/Downloads/NN2.pdf}
    \caption{\textbf{Subtype concordance of Complete feature joined data.}
    Presented here are tiled plots representing the agreement between the
    pre-integration and post integration genefu subtyping analysis results for
    six different tests on the pre- and post-treatment data; in descending order Pam50, ssp2003, ssp2006, iC10,
    scmod1, scmod2 and each column represents one dataset; C1, C2, E1, E2, E3.
    There is significantly more discordance in the subtype agreement than in
    pre-treatment only integration.}
    \label{fig:naive_subs}
\end{sidewaysfigure}


\FloatBarrier


### *ComBat* Integration of Sequential Samples with Multiple Covariates 


\FloatBarrier

Analysis of the subtype composition changes for this integration based on the
covariates resulted in a net decrease in concordance as compared to simple
joins and to the integrated pre-treatment data, Figure \ref{fig:Int13}, page
\pageref{fig:Int13} illustrates this point for the Pam50 subtyping test. The standout result is the large increase in the number of Luminal
A samples post integration in the AI treated cohorts and a small decline in the
number of Basals in the Chemotherapy treated datasets. This is suggestive of a
systematic shift in the centroid mapping algorithm of Luminal B samples
relative expression of certain genes changing enough to be reclassified as
Luminal A. Similarly and possibly more destructively, the Chemotherapy Basal
samples appear to change most frequently to Her2-enriched samples. Table \ref{tab:conc_tab}, page \pageref{tab:conc_tab} contains the number of undistorted samples per *ComBat*
covariate method. *ComBat* performed worse than the Pretreatment only and
uncorrected integration in terms of subtype concordance at 19.6%, 14.2%, 19.1% and 18% unchanged samples for Platform,
Time, Therapy and Subtype integration respectively.

\begin{sidewaysfigure}[htp]
    \includegraphics[width=0.7\textwidth]{/home/richard/Downloads/PVO_Pam50(5).pdf}
    \caption{\textbf{subtype concordance of pre and on-treatment data} Pam50
    subtyping of the pre and on-treatment data before and after integration
    shows a significant difference between the concordance of the pre-treatment
    only integration results (top row) and for each ComBat method (subsequent
    rows). This is especially obvious in E3 the large number of LumA now
    presenting as LumB. This does correlate with later results on the
    concordance of continuous risk scores. }
    \label{fig:Int13}
\end{sidewaysfigure}

&nbsp;

In addition to the categorical tests, continuous risk assessment of the samples
were calculated post integration for each method, and compared to
pre-integration values. Figure \ref{fig:Int14} highlights this result for the rorS
prognostic test, on the integration with only the information of Batch in the
pre and on-treatment data, concurrent results were obtained for each method, were highly concordant, and only
one is being illustrated here for clarity. As Figure \ref{fig:Int14} clearly shows via the loess
regression lines comparing the pre and post integration prognostic values,
there has been a significant shift in the relative risk scoring of the patients
post integration ( > 95% confidence intervals) for all data sets. E2
experienced a small drop in the perceived risk at lower rorS scores, but C1,
C2, E1 and E3 all saw a statistically significant increase in risk for the
better prognostic class patients. At high levels of estimated risk the five
datasets show concurrent reductions in risk post integration. Regardless of
direction this represents a large and systematic divergence of perceived
patient prognostic characteristics. 

\begin{figure}[htp]
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/Rplot58.pdf}
    \caption{\textbf{Continuous risk comparison pre and post integration}
    Comparison of the Pam50 rorS continuous risk assessment scores, here the x
    axis is pre-integration and the y is post-integration shows there is a
    large and systematic shift away from the pre-integration risk values. DS E3
    shows a highly significant increase in the preceived risk of the low risk
    patients, this is consistent with the results of the subtype concordance
    scoring previously which shows a large number of LumA changing to LumB.}
    \label{fig:Int14}
\end{figure}

\FloatBarrier

## Results | Differential Expression Analysis Shows the Changes in Matched Patient Delta Values

The inherent value of this data lies in the information contained in the
pre- and on-treatment gene expression fold changes.
These have previously been shown to help separate response classes to
chemotherapy [@Bownes_2019] and identify markers of response in AI treated
cohorts [@turnbull2015accurate]. Maintaining these changes is of paramount
importance to the value of this work. DE gene lists were calculated using linear
Bayesian models and filtered for significance (FDR < 0.05). These were compared pre
and post integration to evaluate the overlap in retained differentially
expressed genes. This comparison is not drawn for the pretreatment only
integration as there are no pairwise differences across treatment time.



### Uncorrected Combination of Sequentially Sampled Datasets 



\FloatBarrier

Table \ref{tab:DE_tab}, page \pageref{tab:DE_tab} contains the overlap values
for the shared differentially expressed genes present in the reference
unintegrated data and both uncorrected integration methods. Dataset E2 has the
highest agreement with all the correction methods with the independently analysed data, however the remaining data share
comparatively little with their references, with as little as 3.4% of the
same genes being differentially expressed. In addition the complete overlap has
~13% more conserved similarity when compared to the subset feature integration.


\begin{table}[htp]

\centering

\resizebox{\textwidth}{!}{%

\begin{tabular}{|l|l|l|l|l|l|l|l|}

\hline

 & \multicolumn{7}{c|}{Differentially expressed gene list overlap pre- to on-treatment} \\ \hline

 & \multicolumn{1}{c|}{Complete Join} & \multicolumn{1}{c|}{Subset Join} & \multicolumn{1}{c|}{Platform} & \multicolumn{1}{c|}{Time} & \multicolumn{1}{c|}{Therapy} & \multicolumn{1}{c|}{Subtype} & Mean \\ \hline

C1 & 21.2\% & 13.4\% & 22.6\% & 22.6\% & 23.4\% & 22.4\% & 20.9\% \\ \hline

C2 & 32.4\% & 29\% & 27\% & 27\% & 31.8\% & 27\% & 20.9\% \\ \hline

E1 & 42.4\% & 38.4\% & 48.8\% & 48\% & 39.4\% & 47.4\% & 44.7\% \\ \hline

E2 & 91.6\% & 41\% & 50.4\% & 48.2\% & 87.4\% & 49\% & 61.27\% \\ \hline

E3 & 3.4\% & 4.4\% & 5\% & 4.6\% & 4\% & 5\% & 4.4\% \\ \hline

Mean & 38.2\% & 25.5\% & 30.7\% & 30\% & 37\% & 30\% &  \\ \hline

\end{tabular}%

}

\caption{\textbf{Percentage of differentially expressed genes retained after
integration.} Each row contains the percentage of DE genes retained for each
method, for each data set. The summary statistics are listed on the bottom and
right most column.}

\label{tab:DE_tab}

\end{table}


\FloatBarrier


### *ComBat* Integration of Sequential Samples with Multiple Covariates 


\FloatBarrier

Results, Table \ref{tab:DE_tab}, page \pageref{tab:DE_tab}, are similar in magnitude to the pre-treatment attempts at integration but do not
represent an improvement in the retention of biological features, according to
the differentially expressed genes. There is less average similarity across the five datasets
than before and a new minimum similarity (E3/4%). This strongly suggests
that the important patient matched translational changes that are a product of
neoadjuvant treatment are being further lost to *ComBat*. Dataset E2 has the
highest average agreement regardless of method, further identifying the outlier
nature of this dataset.


\FloatBarrier

## Results | Random Forest Classification Evaluates the Degree on Integration from a Data-Driven Perspective

Machine learning methods for the classification of cancers [@he2007ranked] and
the prediction of recurrence of breast cancer [@al2018breast] is well
established. In particular, random forest has been shown to be a robust
algorithm for pattern detection [@al2018breast]. In this study, a random forest was trained on
the pre integration data in order to try and identify the original batches in
each post integration method. By inverting the results of the random forest
output, we can gain a measure of the level of integration based on the mixing
after *ComBat*. Ideally, this results in the “failure” of the RF model to
correctly identify the dataset of origin, indicating that the descriptive
features of each sample have been sufficiently shifted together to appear as
if from one distribution


### Pre-treatment Samples with Platform ComBat Batch Correction


Random forest classification was 17% accurate in the identification of the
original batch in the post integration data. This is below the 28% majority
classifier performance and a clear indication of the successful integration of
expression data. Integration of pre-treatment samples with QN and *ComBat*
show that features descriptive of batch have been eliminated and the
distributions are shifted together well enough to represent a single batch.


### Uncorrected Combination of Sequentially Sampled Datasets 


Naive integration methods showed poor levels of integration according to the
inverse RF metric. The random forest model classified 100% of the Complete
feature joined samples correctly with the batch of origin. The subset
feature joined samples had visible overlap of the E1 and E2 samples and this is
represented in the RF model performance at 82% accuracy. Both of these methods
failed to shift the underlying expression values sufficiently to render the
batch of origin indistinguishable.


### *ComBat* Integration of Sequential Samples with Multiple Covariates 


Random forest mislabeled 142 samples (981/1122, 87%) for the platform
integrated pre and on-treatment samples, 250 (872/1122, 77%) for treatment time
integrated, 400 (722/1122, 64.3%) for the treatment type covariate and  478
samples (644/1122, 57.4%) for subtype adjusted *ComBat* correction. Subtype
integration had the lowest random forest accuracy, correctly identifying the
lowest number of integrated samples but still failed to reach the 28% benchmark. All
*ComBat* assisted integration methods, except the Platform covariate, out
performed the uncorrected method, however they all fell short of the pretreatment
only integration performance.

## Results | Changes In Proliferation On Treatment Reflect the Changes to the Datasets

\FloatBarrier

As proliferation is an important biological marker of response to treatment,
ensuring that the expected reduction to proliferation is still present post
integration is an important metric to validating the integration methods. A
reduction in proliferation markers has long been a marker for response to
therapy [@urruticoechea2005proliferation] and to see a large deviation away
from this would indicate a systematic disturbance to the delta values in the
pre and post treatment samples. Figure \ref{fig:Int15}, page
\pageref{fig:Int15} shows that for each dataset
there is a significant reduction in proliferation markers on treatment compared
to pre-treatment expression levels in the data prior to integration. For all methods, the post
integration on-treatment delta values are not statistically
distinguishable from zero but also have positive means. This indicates that
universally, the starting values for the proliferation markers profiled in this
test (PCNA, MCM2, MKI67, AURKA, FOXM1, BUB1 and TOP2A) have been reduced with
respect to the patient matched on-treatment values or the post treatment
samples appear to now have higher expression than before. The implication of
this result however is that the changes on-treatment no longer represent the
known, normal biological trend of the pre-integration data and are no longer
representative of of these important markers.

\begin{sidewaysfigure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/EXPR_plot.png}
    \caption{\textbf{Changes to normal proliferation values on treatment.}
    Proliferation is normally reduced on-treatment. This figure shows that the normal reductions in
    proliferation are distorted post integration for all methods, here the
    "Base" represents the normal reductions in proliferation in the
    uncorrected, independent, datasets and each other feature illustrates the
    change in proliferation for the ComBat correction methods of the pre- and
    on-treatment data.}
    \label{fig:Int15}
\end{sidewaysfigure}

\FloatBarrier

## Results | Sub-distributions Affect *ComBat* Integration and Are Non-Trivial

\FloatBarrier

To further investigate the possibility of the on-treatment samples causing the
additional noise, Figure \ref{fig:Int16}, page \pageref{fig:Int16} was drawn to
highlight the *ComBat* process of distribution normalization. This diagram
shows the contributions of each
additional dataset to the complexity of the normalization calculations. Each
dataset introduces new distributions that must be shifted and in turn
these increase the complexity of the integration function. It is
possible to see that there are additional "shoulders" in the distributions of
the pre and on-treatment data, indicating bi or multimodal
distributions in the data, which are partially obscured by larger or wider distributions.


\begin{figure}[htp]
    \includegraphics[width=1\textwidth]{/home/richard/Thesis/source/figures/Int1_figs/grouped_figs/Int_PLot.png}
    \caption{\textbf{Evidence of multiple incompatible distributions in the
    pre- and on-treatment data.} It is possible to discern the shoulders of
    additional subdistributions in each dataset for the different combinations
    of datasets that are likely antinomic to the ComBat distribution shifting
    process. }
    \label{fig:Int16}
\end{figure}

The presence of additional sub-distributions was further explored by examining the distributions of the gene expression
on a per-timepoint-per-dataset level. Figure \ref{fig:Int17}, page
\pageref{fig:Int17} shows the mean centered
gene expression distributions for every dataset (A), then the sub distributions
of treatment-by-timepoint as box plots (B), and kernel densities (C). There are
clear distinctions between the time-point-treatment distributions, as seen by the box plots and kernel densities, both of which support the idea that these
additional distributions are limiting the ability of *ComBat* to successfully
integrate the data.

\begin{figure}[htp]
    \includegraphics[width=1\textwidth]{/home/richard/Integration_Rationale/Rplot59.pdf}
    \caption{\textbf{Pre-integration expression distributions and
    subdistributions.} There are clear differences in the overall expression
    profiles of the five different cohorts, A. When accounting not only for the
    study of origin but also the time points, significant splits in the
    expression profiles appear at each on-treatment interval. This suggests
    that treatment is having a profound affect on the overall expression of the
    samples and that each time point represents a new unique distribution. C
    shows this effect as kernel densities and highlights the non-uniformity of
    the data and the number of distinct distributions that must be shifted by
    ComBat}
    \label{fig:Int17}
\end{figure}

\FloatBarrier

## Discussion

There were an enormous number of possible covariates of *ComBat* integration; dataset,
sequencing platform, type of treatment, time on treatment, intrinsic subtype,
known response, histological grade, ER positivity, PR positivity and HER2
status, detection method, BRCA mutation status, node status, age, tumour size
and menopausal status to name just a few. Of these, the factors that were trialled were platform,
treatment time, treatment type, and intrinsic subtype. 
These features were chosen as covariates to improve the performance of
*ComBat* primarily as they were the most feature complete variables for
consideration and would retain the most information. Ideally, these factors
would account for most of the inter-dataset variation and enable the most
uncompromising integration of the data.

&nbsp;

Integration of these disparate cohorts has proved an enormously difficult
undertaking. Batch correction is a fairly standard
procedure[@scherer2009batch], but is possibly insufficiently subtle to combine
on-treatment neoadjuvant data and keep the important and highly sensitive
patient matched delta values intact, despite it's use in the normal removal of
these effects from microarray data [@chen2011removing; @stein2015removing]. Kernel density estimates of the normalized
data for each integration method suggests that integration can be performed
completely as the distributions of the expression data appear as a singular,
uniform, curve with normal parametric characteristics. Importantly, each
component distribution is fully indistinguishable from the others. This is
strongly juxtaposed by the results of the genewise clustering, principal
component analysis, subtyping concordance, random forest analysis, correlation
testing and analysis of the shared differentially expressed genes which all
would indicate that the integration of the data has been highly descriptive to
the underlying data and thus totally unsuccessful. *ComBat* has previously been
shown to improve the statistical analysis of -omic data and improve the
identification of differentially expressed genes [@kupfer2012batch], and
concordance with known biological pathways [@eklund2008correction] but that
seems to falter with the addition of the on-treatment samples.

&nbsp;

Integration of single time point cross platform data, has
been previously described[@Turnbull_2015; @Larsen_2014] and successfully performed with quantile
normalization and *ComBat*. As
expected, the post integration results of this method were the most concordant.
Correlations of the patient samples post integration were comparatively high, >
75% and PCA results of this protocol were visually conforming to the expected
integration outcome. These two tests indicate that the patients still resemble
their pre-integration expression profiles, but the datasets are no longer distinct.
Subtyping and risk scoring showed similar levels of overall similarity with
~80% subtype agreement post integration and a non-significant difference in the
presented risk of recurrence. This is especially important as these metrics are
valuable descriptive features with implicit biological and clinical
implications that need to be maintained to ensure fair analysis post
integration. Discrepancies here would indicate that the post integration data
does not have the same biological representation as the pre-integration data.
Because the resultant subtyping and risk assessment tests are so concordant,
inferences made on the post integration data can still justifiably applied to
the pre-integration samples allowing for integrative analysis. Lastly, supervised
machine learning models were unable to distinguish the study of origin in the
pre-treatment only integrated samples. Overall, this signifies that the
biological diversity of the original data was meaningfully maintained, while
simultaneously removing as much of the technical variance as possible, this is
certainly in keeping with the results from Turnbull *et al.* [@Turnbull_2015]
and other modern examinations of single time point integration retaining
significant biological information[@kupfer2012batch; @eklund2008correction].

&nbsp;

Uncorrected approaches to integration were outperformed by the established
pre-treatment only protocols. Correlations of the cross integration matched
patients samples were lower as a whole with the exception of dataset E2, which
was aberrantly well correlated. Post integration the overall expression values
were not as representative of the pre-integration data, suggesting more
disturbance to the relative gene expression values possibly as a result of the
lack of subtlety in the basic joining methods. The subsequent PCA diagrams
illustrate that, regardless of the change to the underlying relative gene
expression, the inter-dataset variance is maintained. This result indicates
that no technical variance has been removed but the biological values have been altered. Subtype
agreement post integration was very low, just 26.8%. This confirms the result
of the correlation analysis that the relative gene expression values have been
destructively shifted. The post integration subtyping and risk assessment
values are now no longer representative of the unintegrated data making subsequent
analysis of the data irresponsible from this perspective. Additionally, the
pairwise differentially expressed genes were comprised of almost entirely
different genes (38.2% and 25.5% agreement), meaning the valuable treatment-dependent delta values have also been compromised. Lastly, the ML model was
successful at classifying the dataset of origin, thus confirming the results of the
PCA analysis where the inter-dataset variance and differentiating features have
been maintained but the important biological characteristics have been lost. In
context, these results are rational and expected as corrections have long been
established as appropriate protocols for improving cross platform analysis [@engchuan2016handling].

&nbsp;

*ComBat* integration with multiple covariates was outperformed by the
pre-treatment only integration and was roughly on par with the uncorrected
integration methods. Correlation analysis revealed further reduced patient
matched similarity ~60%, again with the exception of dataset E2. This suggests there
is a systematic failure of these methods to alter the general expression
profiles of this dataset, however subsequent analysis fails to reveal a
sufficiently viable reason. Heatmap and PCA analysis of these methods
complement each other to emphasize the fact that the datasets are not
significantly  integrated on any of the highlighted clinical or technical
factors. Additionally, any overlapping of the samples in the heatmaps can be
seen more clearly in the PCA as non-integrative dispersion of the 
data, which scales with the number of covariates. This is a clear illustration of the
fact that *ComBat* assumes uniform contributions of the covariates, a feature
that works against very heterogeneous data. The inability
of *ComBat* to sufficiently handle these non-uniform distributions is
shown in the analysis of sub-distributions in these cohorts, which demonstrate
clear and present multi-modality. Subtyping concordance was on par with the
uncorrected methods, even when starting subtype composition was taken into account,
which strongly indicates that the addition of covariates to integration is adding
little value to this analysis. The pairwise delta values were
maintained only ~32% of the time, meaning the intrinsic values of these samples
is again lost with these methods. Dataset E2 continues to be an outlier in this
analysis, with an average retention of 61.27% of the DE gene lists. Lastly, the
unsupervised machine learning methods had a range of performance in dataset
classification. Fundamentally, this result would indicate an improvement in
integration performance from `Platform < Treatment time < Treatment type <
Subtype`, however with regards to the PCA and heatmap analysis this could
equally be explained by the non-integrative dispersion and possible model
overfitting. There are very few examples of analysis like this and
correspondingly it is very difficult to place the results outside of this
thesis. Muller *et al.* have previously shown that longitudinal integration is
possible using precisely the methodologies employed here, but the successful
integration of this data is
not replicated in this study [@muller2016removing]. This is possibly due to the
nature of the data being substantially different, this data is comprised of
multiple time points from five different platform's, the Muller study was of a
patient cohort with matched patient samples after five years of follow up on
the same platform. 

## Conclusion

Integration of sequentially matched neoadjuvantly treated datasets is a
formidable obstacle to large scale on-treatment breast cancer analysis. Under
the existing standards of data integration, post combination data was not
representative of the preintegration data. This results strongly suggests that
a meta analysis of neoadjuvant therapies is a more appropriate method of
comparing different datasets in a non-destructive manner. At this time, the
standard methods for batch correction and dataset integration via quantile
normalization and *ComBat* correction are inappropriate for patient matched,
neoadjuvant data.



