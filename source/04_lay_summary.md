# Lay-Abstract {.unnumbered}

\footnotesize

\indent
Breast cancer is not one disease but many and there is evidence that each
subtype has distinctly different characteristics. The standard of care for
breast cancer for many years was surgery followed by radiotherapy or more
targeted treatments, if deemed appropriate. A possible alternative approach is
to receive treatment prior to surgery, this method shows promise for helping to
identify patients with good outcomes from patients with poor outcomes. It has
been shown recently to be as effective as the standard of care treatments
but may improve our ability to identify new ways to detect response  in
patients and thus help other people.  

&nbsp;

The most common site of metastasis, and the first place breast cancer spreads,
is to the nearby lymph nodes of the armpit. When tumour cells are detected in
the in the lymph node, patients are known to have worse outcomes and are more
like to have a future recurrence. When tumour is present in lymph nodes, this
information is not used beyond a categorical risk factor for the patient,
however the progression to the lymph nodes and the profile of that tumour may
hold important information for new treatment parameters for each individual. It
is possible that the characteristics inherent to the lymph node tumours that
facilitate the progression from the breast allow these samples to better
represent the risk of the patient. 

&nbsp;

As pre and on-treatment matched samples, and patient matched tissue samples are both rare,
analysis of these samples is often underpowered. Combining available data
for analysis would boost the significance of all findings derived
from this data. However, integration of this data must be undertaken carefully and
with enough subtlety to not inherently transform the underlying biological
information. This proves a non-trivial task but has a large potential
payout for future analysis and biomarkers validation testing.

&nbsp;

These informative matched paired samples have helped identify new methods for
patient risk prediction and combining many smaller studies has improved
our understanding of the differences between different types of patients and
treatment strategies. 

\newpage
