<!-- This page is for an official declaration. -->

\newpage
\normalsize

\vspace*{\fill}

# Declaration {.unnumbered}

\noindent
\textit{
I, Richard Joshua Bownes, confirm that the work presented in this thesis is my own and
has not been submitted elsewhere for any other degree or professional
qualification. Where information has been derived from other sources or in
collaboration with colleagues, I confirm that this has been indicated.  }

\vspace*{\fill}

\newpage

