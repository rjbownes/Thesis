# Acknowledgements {.unnumbered}

\small
This thesis might contain all my own words, but it isn't the product of only
one person. I need to thank my supervisor, Andy Sims, for his constant
supervision, patient and helpful oversight, guidance and experience. I would
not have been able to succeed these last few years with a less capable, or less
kind mentor. Dr. Olga Oikonomidou for co-supervision and for granting access
to the NEO trial which compromised a major portion of my thesis work, and all of
the patients whose samples made all of my work possible. I would also like to
acknowledge my funding body, CRUK, for their generosity.

&nbsp;

\noindent
I would also like to thank my family for their invaluable support. My parents
are my inspiration in life, and have provided help, support and advice beyond
what I can write. During my PhD I have adopted two dogs, who were the missing
limbs I didn't know I didn't have. Bean and Isla have taught me responsibility
in a way only I never knew before. They also helped to get me outside
every once in a while. Lastly my wife, my best friend and the most amazing
woman I know, thank you Max, this would not have happened without you. You have
helped my growth as a student and an academic, you are responsible for my
growth as a person and a husband, and you are my reason to keep to moving.
Check it out, we did it!

\newpage

\vspace*{\fill}




