# Meta-Analysis of Multiple On-treatment Datasets Reveals Common Transcriptional Differences In Non-responsive Tumours

\chaptermark{Integrative Analysis}

## Abstract

\noindent
**Background** 

&nbsp;

\noindent
Analysis of small cohorts of neoadjuvantly treated breast cancer has yielded
some initially powerful results in the area of biomarker identification and
risk stratification of patients. However, these results are undercut by the
limited sample size and possible lack of population representation of the
underlying data. It may be possible to identify more subtle signals in
integrated data and make inferences with significant statistical backing
using the meta analysis of multiple studies of this scarce data. 

&nbsp;

\noindent
**Methods**

&nbsp;

\noindent
Five datasets of transcriptomic breast cancer data and matched annotation
information were collected and combined to create a repository of independently
but homogeneously processed data. The crucial pairwise differences in gene
expression were harvested to identify uniform patterns of change in gene
expression common to all treatment types. Machine learning and neural network
models were trained to predict response from the pairwise delta values.

&nbsp;

\noindent
**Results**

&nbsp;

\noindent
Analysis of the aggregate delta values reveals that there is definite
clustering of the patients based on the factor of non-response regardless of the
treatment involved. Subsequent pathway analysis of these non-responsive samples
reveals a preponderance of genes associated with, and enrichment of, mTOR and
PI3K pathways, suggesting a possible commonality in nonresponse and new targets
and treatments for these patients. ML models were trained to attempt to
identify response status of BC patients cross treatment with significant
accuracy (89% SVM, 92% NN).

&nbsp;

\noindent
**Conclusion**

&nbsp;

\noindent
Examination of matched pre and on-treatment samples facilitates new kinds of
meta analysis from neoadjuvant data, yielding previously unseen insights into
BC treatment response vectors. While endocrine therapy and chemotherapy have
significantly different mechanisms of treatment and this can be seen in the
way patients respond to treatment. The vectors of non-response appear to have
overlap and are enriched for genes currently being targeted for new drug
development, mTOR and PI3K. Lastly, ML methods are able to leverage these
values to train models for the accurate prediction of response across treatment
paradigms by targeting these vectors of non-response.

## Background

There have been previous attempts to to use integrated expression data *en
masse* to enhance the significance of research and provide more powerful
answers to questions posed about patient response prediction and risk
stratification. Multiple algorithms have been developed for correcting the "batch"
effect associated with disparately sourced data and aggregating it to
facilitate new research, these include SVA[@Leek_2007],
BFRM[@Carvalho_2008], FA[@Wang_2011] and AGC[@Kilpinen_2008]. These methods
all seek to shift the distributions of expression towards one another and by
so doing, integrate and normalize the data. *ComBat* [@Johnson_2006] has been established as the
standard method for batch correction and integration of microarray data
[@M_ller_2016; @Chen_2011; @Kupfer_2012], but is, as of yet, untested in matched
patient sequential samples. 

&nbsp;

Recent successful attempts at leveraging the results of multiple translational
studies together have utilized various methods to escape these problems. Jiang
*et al.* constructed a network based approach that individually assessed upstream
involvement of gene expression to identify driver genes to describe specific
gene expression profiles [@Jiang_2019]. This is a popular approach, as there
are several example network analyses of transcriptomic data with associated
genomic quantitative and annotation data by other researchers from the last few
years. Primarily, these studies utilize microarray data from multiple sources, but side step the
problems of batch [@grimes2019integrating]. Other teams have focused on the
integration of expression data with other types of data to perform intricate
multi-omic analysis [@Blazier_2012; @Li_2019]. Tools to
streamline the collection, creation, and annotation of databases from
publicly available repositories have even been produced, however the tight
integration of expression level data is uncommonly attempted and more rarely
successful[@moretto2019first]. 

&nbsp;

To circumvent the problems of batch and multiple expression set alignments, I
have created a unique set of comparable matched pre and on-treatment expression
level change values to make summary analysis of the effects of neoadjuvant
therapy. Here, I examine the results of the scaled pairwise
differences in gene expression to gain new insight into the responses
and changes in breast cancer to treatment. 

## Methods and Materials

The five datasets (GSE59515, GSE20181, GSE87411, GSE122630, GSE32603) were
independently collected and pre-processed to remove genes with no signal or
variance and quality control the samples with Limma [@Limma], DESeq [@DESeq]
and edgeR [@edge1]. The datasets were voom normalized independently and
attempts were made to integrate the expression data using *ComBat* as described
in Chapter 4. As these attempts were unsuccessful, MetaMatchBreast was
constructed using R[@R] and the Bioconductor package XDE[@XDE] establishing an
ExpressionSetList object type to build a frame work of un-merged expression
data with integrated annotation information. This provided an iterable list object
for analysis and testing, with unified pathological and clinical information
for subsetting of the data. 

&nbsp;

Delta values of the pre and on-treatment samples' gene expression values were
used to perform the majority of the analysis as the original data could not be
directly compared. Clustering of the patient-matched delta values was performed
with KNN and PCA to try to identify response associated expression profile
changes. Differential gene expression was calculated using Limma to make
Bayesian inferences about the probability of the gene association to response
class. These genes lists were further used for GSEA[@GSEABase] analysis and
CAMERA[@CAMERA] pathway testing to better understand pan-treatment responses to
therapy. Over-represented genes in the GSEA and CAMERA analysis were collected
and examined for correlated KEGG [@kanehisa2000kegg] pathways.
Python[@van1995python] was used for the ML model building which was trained
using sklearn[@scikit-learn] as the commmon api to access the
SVM[@cortes1995support] model, parameters, and for hyperparameterization.
Tensorflow[@tensorflow2015-whitepaper] and Keras[@chollet2015keras] were used
for building, compilation, and training of a sequential convolution neural
network for the prediction of response class from the pairwise delta values.  


## Results

### General Changes in Gene Expression are Conserved Between Treatments

\FloatBarrier

Differentially expressed genes were identified as previously described
[@Bownes_2019] with an fdr of 0.05 from the matched pre and on-treatment
samples in the Chemotherapy cohort. The genes lists defined by this method were
probed in the endocrine treated cohorts to view the similarity in expression
level change. This analysis shows similar patterns of expression in both cohorts, however
with statistically significant differences (intergroup means p <
0.05 standard t-Test) in the amplitude of change, see Figure \ref{fig:MMB1},
page \pageref{fig:MMB1}. This means that genes that
become downregulated post treatment in the Chemotherapy patients also see a
reduction in expression in the AI treated patients but to a greater extent.
This indicates that the response to treatment is similar but possible pathway
involvement and specific methods of action are different between the two
treatment types. This result shows that many of the same changes occur in Chemotherapy as in
Endocrine therapy and separation of response or treatment specific changes
will need to be examined more subtly.


\begin{sidewaysfigure}[htp]
\centering
    \includegraphics[width=0.8\textwidth]{/home/richard/Downloads/MMB_DE2.pdf}
    \caption{\textbf{Comparative expression of DE genes from matched pre- and
    on-treatment samples.} Genes that were differentially expressed between the
    pre and on-treatment samples in the Chemotherapy cohorts show a larger
    degree of differential expression compared to their corresponding
    expression in the AI cohorts. Direction of expression is constant but
    magnitude is different. Genes were identified using Limma and a Bayesian
    framework with decision trees to identify 46 significantly differentially
    expressed features.}
    \label{fig:MMB1}
\end{sidewaysfigure}


\FloatBarrier

### Non-Response Vectors of Chemotherapy and Endocrine Show Concordance On-treatment

\FloatBarrier


Pre to on-treatment delta values of matched patients cluster separately on
the basis of treatment and response when viewed as feature reduced t-SNE
representations. Responsive Chemotherapy and responsive Endocrine therapy patients
clearly cluster away from the non-responsive samples, which appear to have a
more similar variance, Figure \ref{fig:MMB2}, page \pageref{fig:MMB2}. This is possibly suggestive of differing pathways of
response to different therapies but common pathways of non-response. This
possibly indicates that non-responsiveness can be predicted using the pairwise
differential gene scores. 


\begin{figure}[htp]
\centering
    \includegraphics[width=1\textwidth]{/home/richard/Integration_Rationale/Rplot63.pdf}
    \caption{\textbf{t-SNE of BC transcriptional pairwise values shows
    clustering of patients by categorical response.}
    t-SNE shows clear clustering of groups by the joint grouping of response
    and treatment but with overlap of the non-responsive endocrine and
    chemotherapy patients. t-SNE is a probabalistic method of expressing
    similarity, hence this results suggests that the groupings visible here are
    due to inherent similarity of the samples in each cluster. The data in this
    model are the pairwise expression level changes of the on-treatment samples
    relative to the pre-treatment.}
    \label{fig:MMB2}
\end{figure}


&nbsp;

To enumerate the differences between the response classes, I plotted the D1/D2
contributions for each sample and made statistical tests between each group to
see which were significantly distant, Figure \ref{fig:MMB2a}, page \pageref{fig:MMB2a}. In the first dimension, the non-responsive
samples are indistinguishable from each other but are significantly
separated from the responsive samples from either treatment paradigm. In the
second dimension, there is significant separation between all response-treatment
classes. The statistical differences presented in this analysis are highly
suggestive that there are systematic differences between the non-responders and
the other classes and between the response classes.

\begin{figure}[htp]
\centering
    \includegraphics[width=1\textwidth]{/home/richard/Integration_Rationale/Rplot68.pdf}
    \caption{\textbf{Statistical analysis of intergroup differences show
    significant seperaration on both principal components.}
    Significance scores for the intergroup means differences are presented with stars for short hand for the first dimension, left, and the second, right.}
    \label{fig:MMB2a}
\end{figure}


\FloatBarrier

### Pathway Analysis Highlights Conserved Genes Indicative of Non-response Pan-treatment

\FloatBarrier

t-SNE representation shows there is a significant clustering of the
non-responsive patients pan-treatment using the pairwise delta values of the
pre and on-treatment samples as input. In order to establish whether there was
a significant biological underpinning for the gene lists, pathway and network
analysis were performed and the results can be seen in Figure \ref{fig:MMB3},
page \pageref{fig:MMB3}. This result is suggestive of common expression profile
changes in patients with no response to
treatment not present in either of the responsive classes. Combination network
and gene set enrichment score analysis filtered gene lists revealed
significant enrichment of mTOR and PI3K KEGG pathways. Analysis of the surrounding literature of
non-responsive and TNBC breast cancer
reveals mTOR and kinase inhibitors to be of special interest to the treatment of
these diseases. A further examination of the gene lists was undertaken to
examine the gene specific changes that contribute to this result. There were
significant differences in several of the genes constituting the differential
feature list used for the pathway analysis, Figure \ref{fig:MMB3a}. This
supports the pathway analysis and t-SNE diagrams that the pre- and on-treatment
expression level change values are capturing patient specific response.

\begin{figure}[htp]
\centering
    \includegraphics[width=1\textwidth]{/home/richard/Integration_Rationale/Rplot67.pdf}
    \caption{\textbf{KEGG pathway enrichment scores.}
    KEGG enrichment shows an increased enrichment and number of genes in the
    non-responders not seen in the responsive samples.}
    \label{fig:MMB3}
\end{figure}


\begin{figure}[htp]
\centering
    \includegraphics[width=1\textwidth]{/home/richard/Integration_Rationale/Rplot75.pdf}
    \caption{\textbf{Differentially expressed genes show significant
    differences between response classes}
    MDM2 shows an increase in expression level on-treatment in the
    non-responding patients, while a proliferation associated marker RBL2 shows
    a decrease on treatment in the responders, but a steady level in the
    non-responders. These are two of the genes that see significant
    differential expression between the overall responsive classes and all
    labeled non-responders.}
    \label{fig:MMB3a}
\end{figure}


\FloatBarrier

### Identification of Non-responsive Patients from Pairwise Delta Expression Values

\FloatBarrier

In order to determine if the aggregated differential gene lists from the pre-
to on-treatment matched patient expression levels changes could be used for the
accurate prediction of response, two models were trained to predict response
independent of treatment. Samples with available response status were subset into an 80:20
training and test split and two models were trained on the response classes,
regardless of treatment paradigm, using the differential gene lists to create a
predictive model. As the
classes were well balanced accuracy could have been used as the test metric,
but to ensure fair testing the precision, recall, and F1 scores are reported in Table
\ref{tab:MMBt1}, page \pageref{tab:MMBt1}. SVM using a radial boosting function
was trained to find the boundary of the response class areas and had an F1 score
of 0.91 in training and 0.89 in testing on our split data.  In addition, a
neural network was constructed using Tensorflow and Keras with two dense layers
and a 0.1 fractional dropout layer to avoid over fitting, which resulted in a
training accuracy of 0.97 and a test validation of 0.92. These models were both
highly accurate at detecting overall response from these pre- and on-treatment
matched expression level values, clearly indicating that the relative changes
seen on-treatment are important for stratifying risk and predicting patient
response. 


\begin{table}[htp]

\centering

\resizebox{\textwidth}{!}{%

\begin{tabular}{|l|l|l|l|l|l|l|}

\hline

Model & Precision & Recall & F1 & SVM & P.Responder & P.Non-Responder \\ \hline

SVM & 0.88 & 0.9 & 0.89 & Responder & 180 & 24 \\ \hline

NN & 0.92 & 0.93 & 0.92 & Non-Responder & 20 & 161 \\ \hline

\multicolumn{4}{|l|}{\multirow{3}{*}{}} & NN & P.Responder & P.Non-Responder \\
\cline{5-7} 

\multicolumn{4}{|l|}{} & Responder & 190 & 16 \\ \cline{5-7} 

\multicolumn{4}{|l|}{} & Non-Responder & 14 & 165 \\ \hline

\end{tabular}%

}

\caption{\textbf{Machine learning models report high accuracy for predicting
response from matched sequential samples.} SVM
and NN's were built and trained on an 80/20 split to identify response. The
precision, recall, and F1 are listed in the table.}

\label{tab:MMBt1}

\end{table}

\FloatBarrier

## Discussion

The results of this analysis reveal a few potentially important and suggestive
outcomes as to the net change experienced as part of normal neoadjuvant therapy
of breast cancer. Previous work has highlighted that there are important genes
differentially expressed in response to chemotherapy[@buchholz2002global;
@modlich2004immediate] and  endocrine therapy[@dowsett2005short] and that it is
clear that these lists are not identical. Common threads are a decrease in the
level of proliferation markers [@dowsett2006proliferation; @chang2000apoptosis]
and apoptosis in both paradigms. However, the initial analysis of the changes
seen in chemotherapy are mirrored, to a lesser extent in the endocrine cohorts
suggesting some commonality in the overall expression profile of antimitotic
therapy, regardless of target.

&nbsp;

Possibly of more clincial interest is the perceived overlap in the trajectory
of gene expression for non-responsive patients in both treatment type cohorts.
While the pathways of response differ between AI and CT, the pathways of
non-response show greater similarity and unsupervised analysis reveals them to
appear to be the same sub-population. This is potentially of great interest to
novel drug discovery, as this could potentially lead to new targeted
therapies. This statement is emboldened by the results of the differential gene
expression analysis, pathway enrichment, and gene set scoring methods, which
show conserved similarity in the pathways between total response and
non-responsive patients. KEGG analysis showed significant enrichment of the
PI3K and mTOR pathways; two promising contemporary therapies to more resistant
tumours [@juvekar2012combining; @juvekar2012combining; @costa2018targeting].

&nbsp;

Lastly, models were built
using standard statistical machine learning and neural network approaches for
the identification of response from the on-treatment expression vectors with
marked success in testing (SVM: 89%, NN: 92%). This is highly concordant with
the accuracy observed in previous studies of applied ML to predicting risk and
recurrence from clinical and pathological features (decision trees, CNN and SVM are 0.936, 0.947 and 0.957 accuracy
respectively)[@eshlaghy2013using] and imaging data [@khan2019novel]. This is further support for the work
detailed in Chapter 2, which began to enumerate the value of treatment samples
for this precise purpose. These results are of imminent interest to the body of work surrounding this text in the
area of biomarker identification and risk stratification.


## Conclusion

Meta-analysis of multiple pre- and on-treatment neoadjuvantly treated datasets
has revealed several key insights into the value of sequential sampling and the
applicability of pan-treatment markers for response in primary breast cancer.
*En masse* some of the changes seen in one treatment are seen in the others and
the pairwise differences observed in different response classes can be utilized
to predict response from the pairwise delta values captured by sequential
sampling. Patients that respond to chemotherapy and patients that respond to
endocrine therapy have different relative gene expression profiles, but the
non-responders to both theraies show strong overlap in relative expression from
the sequential samples. Previously identified gene pathways in BRCA-1 and TNBC tumours were
enriched in the non-responsive tumour groups. Lastly, supervised machine
learning algorithms trained on the sequential delta values were highly accurate
for the prediction of response in both the neoadjuvant chemotherapy and
endocrine settings.
