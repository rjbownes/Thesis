<!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
        <title>Template for writing a PhD thesis in Markdown [![Build Status](https://travis-ci.org/tompollard/phd_thesis_markdown.svg?branch=master)](https://travis-ci.org/tompollard/phd_thesis_markdown)</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0/dist/katex.min.css" integrity="sha384-9eLZqc9ds8eNjO3TmqPeYcDj8n+Qfa4nuSiGYa6DjLNcv9BtN69ZIulL9+8CqC9Y" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Microsoft/vscode/extensions/markdown-language-features/media/markdown.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Microsoft/vscode/extensions/markdown-language-features/media/highlight.css">
        <link href="https://cdn.jsdelivr.net/npm/katex-copytex@latest/dist/katex-copytex.min.css" rel="stylesheet" type="text/css">
        <style>
.task-list-item { list-style-type: none; } .task-list-item-checkbox { margin-left: -20px; vertical-align: middle; }
</style>
        <style>
            body {
                font-family: -apple-system, BlinkMacSystemFont, 'Segoe WPC', 'Segoe UI', 'Ubuntu', 'Droid Sans', sans-serif;
                font-size: 14px;
                line-height: 1.6;
            }
        </style>
        
        <script src="https://cdn.jsdelivr.net/npm/katex-copytex@latest/dist/katex-copytex.min.js"></script>
    </head>
    <body>
        <h1 id="template-for-writing-a-phd-thesis-in-markdown-build-status">Template for writing a PhD thesis in Markdown <a href="https://travis-ci.org/tompollard/phd_thesis_markdown"><img src="https://travis-ci.org/tompollard/phd_thesis_markdown.svg?branch=master" alt="Build Status"></a></h1>
<p><a href="http://dx.doi.org/10.5281/zenodo.58490"><img src="https://zenodo.org/badge/doi/10.5281/zenodo.58490.svg" alt="DOI"></a></p>
<p>This repository provides a framework for writing a PhD thesis in Markdown. I used the template for my PhD submission to University College London (UCL), but it should be straightforward to adapt suit other universities too.</p>
<h2 id="citing-the-template">Citing the template</h2>
<p>If you have used this template in your work, please cite the following publication:</p>
<blockquote>
<p>Tom Pollard et al. (2016). Template for writing a PhD thesis in Markdown. Zenodo. <a href="http://dx.doi.org/10.5281/zenodo.58490">http://dx.doi.org/10.5281/zenodo.58490</a></p>
</blockquote>
<h2 id="why-write-my-thesis-in-markdown">Why write my thesis in Markdown?</h2>
<p>Markdown is a super-friendly plain text format that can be easily converted to a bunch of other formats like PDF, Word and LaTeX. You'll enjoy working in Markdown because:</p>
<ul>
<li>it is a clean, plain-text format...</li>
<li>...but you can use LaTeX when you need it (for example, in laying out mathematical formula).</li>
<li>it doesn't suffer from the freezes and crashes that some of us experience when working with large, image-heavy Word documents.</li>
<li>it automatically handles the table of contents, bibliography etc with Pandoc.</li>
<li>comments, drafts of text, etc can be added to the document by wrapping them in &lt;!--  --&gt;</li>
<li>it works well with Git, so keeping backups is straightforward. Just commit the changes and then push them to your repository.</li>
<li>there is no lock-in. If you decide that Markdown isn't for you, then just output to Word, or whatever, and continue working in the new format.</li>
</ul>
<h2 id="are-there-any-reasons-not-to-use-markdown">Are there any reasons not to use Markdown?</h2>
<p>There are some minor annoyances:</p>
<ul>
<li>if you haven't worked with Markdown before then you'll find yourself referring to the style-guide fairly often at first.</li>
<li>it isn't possible to add a short caption to tables <s>and figures</s> (<a href="https://github.com/tompollard/phd_thesis_markdown/pull/47">figures are now fixed</a>, thanks to @martisak). This means that /listoftables includes the long-caption, which probably isn't what you want. If you want to include the list of tables, then you'll need to write it manually.</li>
<li>the style documents in this framework could be improved. The PDF and HTML (thanks <a href="https://github.com/ArcoMul">@ArcoMul</a>) outputs are acceptable, but <s>HTML and</s> Word needs work if you plan to output to this format.</li>
<li><s>there is no straightforward way of specifying image size in the markdown right now, though this functionality is coming (see: <a href="https://github.com/tompollard/phd_thesis_markdown/issues/15">https://github.com/tompollard/phd_thesis_markdown/issues/15</a>)</s> (Image size can now be specified. Thanks to @rudolfbyker for <a href="https://github.com/tompollard/phd_thesis_markdown/issues/15">highlighting this</a>).</li>
<li>... if there are more, please add them here.</li>
</ul>
<h2 id="how-is-the-template-organised">How is the template organised?</h2>
<ul>
<li><a href="http://README.md">README.md</a> =&gt; these instructions.</li>
<li><a href="http://License.md">License.md</a> =&gt; terms of reuse (MIT license).</li>
<li>Makefile =&gt; contains instructions for using Pandoc to produce the final thesis.</li>
<li>output/ =&gt; directory to hold the final version.</li>
<li>source/ =&gt; directory to hold the thesis content. Includes the references.bib file.</li>
<li>source/figures/ =&gt; directory to hold the figures.</li>
<li>style/ =&gt; directory to hold the style documents.</li>
</ul>
<h2 id="how-do-i-get-started">How do I get started?</h2>
<ol>
<li>
<p>Install the following software:</p>
<ul>
<li>A text editor, like <a href="https://www.sublimetext.com/">Sublime</a>, which is what you'll use write the thesis.</li>
<li>A LaTeX distribution (for example, <a href="https://tug.org/mactex/">MacTeX</a> for Mac users).</li>
<li><a href="http://johnmacfarlane.net/pandoc">Pandoc</a>, for converting the Markdown to the output format of your choice.  You may also need to install <a href="http://pandoc.org/demo/example19/Extension-citations.html">Pandoc cite-proc</a> to create the bibliography.</li>
<li>Install @martisak's shortcaption module for Pandoc, with <code>pip install pandoc-shortcaption</code></li>
<li>Git, for version control.</li>
</ul>
</li>
<li>
<p><a href="https://github.com/tompollard/phd_thesis_markdown/fork">Fork the repository</a> on Github</p>
</li>
<li>
<p>Clone the repository onto your local computer (or <a href="https://github.com/tompollard/phd_thesis_markdown/archive/master.zip">download the Zip file</a>).</p>
</li>
<li>
<p>Navigate to the directory that contains the Makefile and type &quot;make pdf&quot; (or &quot;make html&quot;) at the command line to update the PDF (or HTML) in the output directory.<br>
<strong>In case of an error</strong> (e.g. <code>make: *** [pdf] Error 43</code>) run the following commands:</p>
<pre><code><div>sudo tlmgr install truncate
sudo tlmgr install tocloft
sudo tlmgr install wallpaper
sudo tlmgr install morefloats
sudo tlmgr install sectsty
sudo tlmgr install siunitx
sudo tlmgr install threeparttable
sudo tlmgr update l3packages
sudo tlmgr update l3kernel
sudo tlmgr update l3experimental
</div></code></pre>
</li>
<li>
<p>Edit the files in the 'source' directory, then goto step 4.</p>
</li>
</ol>
<h2 id="what-else-do-i-need-to-know">What else do I need to know?</h2>
<p>Some useful points, in a random order:</p>
<ul>
<li>each chapter must finish with at least one blank line, otherwise the header of the following chapter may not be picked up.</li>
<li>add two spaces at the end of a line to force a line break.</li>
<li>the template uses <a href="http://johnmacfarlane.net/pandoc/README.html">John Macfarlane's Pandoc</a> to generate the output documents. Refer to this page for Markdown formatting guidelines.</li>
<li>PDFs are generated using the LaTeX templates in the style directory. Fonts etc can be changed in the TeX templates.</li>
<li>To change the citation style, just overwrite ref_format.csl with the new style. Style files can be obtained from <a href="http://citationstyles.org/">citationstyles.org/</a></li>
<li>For fellow web developers, there is a Grunt task file (Gruntfile.js) which can be used to 'watch' the markdown files. By running <code>$ npm install</code> and then <code>$ npm run watch</code> the PDF and HTML export is done automatically when saving a Markdown file.</li>
<li>You can automatically reload the HTML page on your browser using LiveReload with the command <code>$ npm run livereload</code>. The HTML page will automatically reload when saving a Markdown file after the export is done.</li>
</ul>
<h1 id="contributing">Contributing</h1>
<p>Contributions to the template are encouraged! There are lots of things that could be improved, like:</p>
<ul>
<li>finding a way to add short captions for the tables, so that the lists of tables can be automatically generated.</li>
<li>cleaning up the LaTeX templates, which are messy at the moment.</li>
<li>improving the style of Word and TeX outputs.</li>
</ul>
<p>Please fork and edit the project, then send a pull request.</p>

    </body>
    </html>