# Study Structure {.unnumbered}

This thesis is comprised of several distinct sections. Firstly an introduction
to preface the topic and outline the problems of breast cancer to establish
familiarity with the contents of my research. Four results chapters that detail
the output of this thesis follow, then a discussion and conclusion, which seek
to tie the threads of my work together and firmly establish its position in the
corpus of scientific literature. Results chapter one is a reprinting of my
first author paper with permission from the journal. The contents of the second
results chapter are currently submitted for peer review and publication, any
amendments or corrections that are required for publication will be included
with any necessary corrections to this thesis as a result of my viva. Chapters
3 and 4 have been conducted in parallel but will be presented as two separate
sections: Chapter 3 dictating the methods involved to create and verify the
integration of multiple different datasets and the creation of
MetaMatchedBreast, followed by Chapter 4 detailing the novel results derived
from the analysis of this meta-dataset. The results of Chapter 5 and the
pre-requisite information to justify the combination of these neoadjuvant
datasets will be coerced into a single manuscript for future submission.


