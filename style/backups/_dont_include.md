# Appendix 1: Additional Figures {.unnumbered}


\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Thesis/source/figures/Int1_figs/Platform/mammaprint_difference.png}
    \caption{\textbf{Starting Subtype Composition.} support text}
    \label{fig:s1}
\end{figure}

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Thesis/source/figures/Int1_figs/TreatmentType/mammaprint_difference.png}
    \caption{\textbf{Starting Subtype Composition.} support text}
    \label{fig:s2}
\end{figure}

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Thesis/source/figures/Int1_figs/TreatmentTime/mammaprint_difference.png}
    \caption{\textbf{Starting Subtype Composition.} support text}
    \label{fig:s3}
\end{figure}

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Thesis/source/figures/Int1_figs/Subtype/mammaprint_difference.png}
    \caption{\textbf{Starting Subtype Composition.} support text}
    \label{fig:s4}
\end{figure}


\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Thesis/source/figures/Int1_figs/Baseline/genenumbers.png}
    \caption{\textbf{genes} support text}
    \label{fig:s5}
\end{figure}


\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/p001937_rors.pdf}
    \caption{\textbf{.}.}
    \label{fig:s6}
\end{figure}

\begin{figure}[htp]
\centering
    \includegraphics[width=0.9\textwidth]{/home/richard/Integration_Rationale/p09151_mamma.pdf}
    \caption{\textbf{.}.}
    \label{fig:s7}
\end{figure}







