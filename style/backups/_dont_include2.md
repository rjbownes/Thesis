# Appendix 2: Additional Data {.unnumbered}

<!-- 
This could include extra figures or raw data
-->


\begin{figure}[htp]
    \includegraphics[width=0.7\textwidth]{/home/richard/Integration_Rationale/Rplot35.pdf}
    \caption{\textbf{Pretreatment only integration rorS agreement diagram}
    Diagram of the pairwise differences in the presentation of the continuous
    risk assesment scores for the pre-treatment only integration samples.}
    \label{fig:Int_pre_risk}
\end{figure}

\begin{landscape}

\begin{figure}[htp]
    \includegraphics[width=0.7\textwidth]{/home/richard/Integration_Rationale/Rplot36.pdf}
    \caption{\textbf{Complete feature joined integration rorS agreement diagram}
     Diagram of the pairwise differences in the presentation of the continuous
    risk assesment scores for the complete feature joined integration samples.}
    \label{fig:Int_complete_risk}
\end{figure}

\begin{landscape}
